package com.blagoseng.constants.strings;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 07.06.13
 * Time: 20:55
 */
public final class Strings {
    public static final String DOCUMENTS_MENU = "Документы";
    public static final String OPERATIONS_MENU = "Операции";
    public static final String CATALOGS_MENU = "Справочники";
    public static final String SETTINGS_MENU = "Настройки";

    public static final String INCOMING_MENU_ITEM = "Приход";
    public static final String OUTGO_MENU_ITEM = "Расход";
    public static final String RETURN_MENU_ITEM = "Возврат";

    public static final String INCOMING_BILLS_MENU_ITEM = "Приходные накладные";
    public static final String OUTGO_BILLS_MENU_ITEM = "Расходные накладные";
    public static final String RETURN_BILLS_MENU_ITEM = "Возвратные накладные";

    public static final String STAFF_CATALOG_MENU_ITEM = "Сотрудники";
    public static final String PRODUCT_CATALOG_MENU_ITEM = "Товары";
    public static final String UNITS_CATALOG_MENU_ITEM = "Единицы измерения";
    public static final String POSTS_CATALOG_MENU_ITEM = "Должности";
    public static final String CATEGORIES_CATALOG_MENU_ITEM = "Категории товаров";
    public static final String OBJECTS_CATALOG_MENU_ITEM = "Объекты";

    public static final String ACTION_ADD = "Добавить";
    public static final String ACTION_EDIT = "Редактировать";
    public static final String ACTION_DELETE = "Удалить";

    public static final String ADD_BUTTON = "Добавить";
    public static final String UPDATE_BUTTON = "Изменить";
    public static final String CANCEL_BUTTON = "Отмена";

    public static final String DIALOG_CAPTION = "Добавление записи в ";

    public static final class SQL {
        public static final String SELECT = "SELECT ";
        public static final String DELETE = "DELETE FROM ";
        public static final String INSERT = "INSERT INTO ";
        public static final String UPDATE = "UPDATE ";
        public static final String SET = " SET ";
        public static final String VALUES = " VALUES ( ";
        public static final String FROM = " FROM ";
        public static final String WHERE = " WHERE ";
        public static final String IS = "=";

        public static final String START_BRACKET = " (";
        public static final String END_BRACKET = ") ";
        public static final String SEMICOLON = ";";
        public static final String BACK_QUOTE = "'";
        public static final String COMMA = ", ";
        public static final String ALL = " * ";
    }
}
