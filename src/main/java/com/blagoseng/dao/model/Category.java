package com.blagoseng.dao.model;

import com.blagoseng.dao.interfaces.Accessable;
import com.blagoseng.interfaces.HasName;

import java.io.Serializable;
import java.lang.*;
import java.lang.Object;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:21
 */
public class Category implements Serializable, Accessable, HasName {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "[ Category \n"
                + "   Id:" + id + "\n"
                + " Name: " + name + " ]\n";
    }

    @Override
    public Object getPropertyByIndex(int index) {
        switch (index ) {
            case 0: return id;
            case 1: return name;
            default:return null;
        }
    }
}
