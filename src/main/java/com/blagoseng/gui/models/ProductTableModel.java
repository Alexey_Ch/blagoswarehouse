package com.blagoseng.gui.models;

import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.MySQLDAOFactory;
import com.blagoseng.dao.model.Category;
import com.blagoseng.dao.model.Product;
import com.blagoseng.dao.model.Unit;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import com.blagoseng.utils.ListUtils;
import org.apache.log4j.Logger;

import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 11.07.13
 * Time: 23:59
 */
public class ProductTableModel extends AbstractTableModel{
    private static final Logger LOG = Logger.getLogger(ProductTableModel.class);

    public ProductTableModel() {
        factory = DAOFactory.getMySQLDAOFactory();
        loadData();
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object;
        Product product = list.get(rowIndex);
        switch (columnIndex) {
            case 0: {object = product.getName(); break;}
            case 1: {object = product.getLabel(); break;}
            case 2: {object = ListUtils.getCategoryById(categoryList, product.getCategoryId()).getName(); break;}
            case 3: {object = ListUtils.getUnitById(Arrays.asList(unitList), product.getUnitId()).getShortName(); break;}
            default: object = null;
        }
        return object;
    }

    @Override
    public String getColumnName(int column) {
        return DatabaseUtils.getColumnNamesForTable(DatabaseSchema.TABLE_PRODUCTS)[column + 1];
    }

    private void loadData() {
        list = factory.getProductDAO().getProductList();
        unitList = factory.getUnitDAO().getUnits();
        categoryList = factory.getCategoryDAO().getAllCategories();
    }

    public Product getProduct(int row) {
        return list.get(row);
    }

    public void updateModel() {
        loadData();
        fireTableDataChanged();
    }

    private List<Product> list;
    private Unit[] unitList;
    private List<Category> categoryList;
    private MySQLDAOFactory factory;
}
