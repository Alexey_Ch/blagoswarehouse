package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.Object;

import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 13:03
 */
public interface ObjectDAO {
    public ResultSet selectObjectResultSet();
    public boolean createObject(Object object);
    public boolean deleteObject(int objectId);
    public boolean updateObject(Object object);

    public Object getObject(int objectId);
    public Object[] getObjects();
}

