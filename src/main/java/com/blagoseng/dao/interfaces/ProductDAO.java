package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.Post;
import com.blagoseng.dao.model.Product;

import java.sql.ResultSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 13:02
 */
public interface ProductDAO {
    public ResultSet selectProductResultSet();
    public boolean insertProduct(Product product);
    public boolean deleteProduct(int productId);
    public boolean updateProduct(Product product);

    public List<Product> getProductList();
    public Product getProduct(int productId);
}
