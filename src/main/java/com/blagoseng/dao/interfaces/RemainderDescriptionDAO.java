package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.RemainderDescription;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 11.07.13
 * Time: 22:30
 */
public interface RemainderDescriptionDAO {
    public List<RemainderDescription> getRemainderDescriptionList(int itemId);
}
