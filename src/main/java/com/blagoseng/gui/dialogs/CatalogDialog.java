package com.blagoseng.gui.dialogs;

import com.blagoseng.constants.dimensions.Dimensions;
import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.MySQLDAOFactory;
import com.blagoseng.dao.model.*;
import com.blagoseng.gui.models.ResultSetTableModel;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import com.blagoseng.utils.ImageUtils;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 1:43
 */
public class CatalogDialog extends JDialog {
    private static final Logger LOG = Logger.getLogger(CatalogDialog.class.getName());

    public CatalogDialog(JFrame frame, boolean modal, String tableName) {
        super(frame, modal);
        this.tableName = tableName;
        setTitle(DatabaseUtils.getTableTitle(tableName));
        add(createToolBar(), BorderLayout.NORTH);
        data = new JTable();
        loadData();
        add(new JScrollPane(data), BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(null);
    }

    public void loadData() {
        MySQLDAOFactory mysql = (MySQLDAOFactory) DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        ResultSet resultSet = null;
        AbstractTableModel tableModel = null;
        String[] columnNames =  DatabaseUtils.getColumnNamesForTable(tableName);

        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_UNITS)) {
            resultSet =   mysql.getUnitDAO().selectUnitResultSet();
            tableModel = new ResultSetTableModel(resultSet, columnNames);
            data.setModel(tableModel);
            data.removeColumn(data.getColumnModel().getColumn(0));
        } else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_EMPLOYERS)) {
            resultSet =  mysql.getEmployerDAO().selectEmployerResultSet();
            tableModel = new ResultSetTableModel(resultSet, columnNames);
            data.setModel(tableModel);
            data.removeColumn(data.getColumnModel().getColumn(0));
        } else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_POSTS)) {
            resultSet =  mysql.getPostDAO().selectPostResultSet();
            tableModel = new ResultSetTableModel(resultSet, columnNames);
            data.setModel(tableModel);
            data.removeColumn(data.getColumnModel().getColumn(0));
        } else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_CATEGORIES)) {
            resultSet = mysql.getCategoryDAO().getCategoryResultSet();
            tableModel = new ResultSetTableModel(resultSet, columnNames);
            data.setModel(tableModel);
            data.removeColumn(data.getColumnModel().getColumn(0));
        }
        else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_PRODUCTS)) {
            resultSet =  mysql.getProductDAO().selectProductResultSet();
            tableModel = new ResultSetTableModel(resultSet, columnNames);
            data.setModel(tableModel);
            data.removeColumn(data.getColumnModel().getColumn(1));
        }
        else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_OBJECTS)) {
            resultSet =  mysql.getObjectDAO().selectObjectResultSet();
            tableModel = new ResultSetTableModel(resultSet, columnNames);
            data.setModel(tableModel);
            data.removeColumn(data.getColumnModel().getColumn(0));
        }
        data.setRowHeight(30);
        data.repaint();
    }

    private boolean deleteItem(int selectedItem) {
        boolean result = false;
        int id;
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_UNITS)) {
            id = (Integer) data.getModel().getValueAt(selectedItem, 0);
            LOG.debug("method: deleteItem -> unit id = " + id);
            Unit unit = new Unit();
            unit.setId(id);
            DAOFactory.getMySQLDAOFactory().getUnitDAO().deleteUnit(unit);
        } else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_EMPLOYERS)) {
            id = (Integer) data.getModel().getValueAt(selectedItem, 0);
            LOG.debug("method: deleteItem -> employer id = " + id);
            DAOFactory.getMySQLDAOFactory().getEmployerDAO().deleteEmployer(id);
        } else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_POSTS)) {
            try {
                ((ResultSetTableModel) data.getModel()).getResultSet().beforeFirst();
            } catch (SQLException e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
            }
            id = (Integer) data.getModel().getValueAt(selectedItem, 0);
            LOG.debug("method: deleteItem -> post id = " + id);
            DAOFactory.getMySQLDAOFactory().getPostDAO().deletePost(id);
        } else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_CATEGORIES)) {
            id = (Integer) data.getModel().getValueAt(selectedItem, 0);
            LOG.debug("method: deleteItem -> category id = " + id);
            DAOFactory.getMySQLDAOFactory().getCategoryDAO().deleteCategory(id);
        }
        else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_PRODUCTS)) {
            id = (Integer) data.getModel().getValueAt(selectedItem, 1);
            LOG.debug("method: deleteItem -> product id = " + id);
            DAOFactory.getMySQLDAOFactory().getProductDAO().deleteProduct(id);
        }
        else
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_OBJECTS)) {
            id = (Integer) data.getModel().getValueAt(selectedItem, 0);
            LOG.debug("method: deleteItem -> product id = " + id);
            DAOFactory.getMySQLDAOFactory().getObjectDAO().deleteObject(id);
        }
        return result;
    }

    private JToolBar createToolBar() {
        JToolBar editToolBar = new JToolBar();
        editToolBar.setLayout(new GridLayout(1, 3));
        Dimension dim = new Dimension(Dimensions.TOOLBAR_BUTTON_WIDTH,
                Dimensions.TOOLBAR_BUTTON_HEIGHT);
        Dimension iconSize = new Dimension(Dimensions.TOOLBAR_ICON_WIDTH, Dimensions.TOOLBAR_BUTTON_HEIGHT);
        JButton add = new JButton(Strings.ACTION_ADD);
        add.setPreferredSize(dim);
        add.addActionListener(new NewAction());
        JButton edit = new JButton(Strings.ACTION_EDIT);
        edit.setPreferredSize(dim);
        edit.addActionListener(new EditAction());
        JButton delete = new JButton(Strings.ACTION_DELETE);
        delete.setPreferredSize(dim);
        delete.addActionListener(new DeleteAction());
        try {
            BufferedImage icon = ImageIO.read(new File("./assets/icons/add.png"));
            add.setIcon(new ImageIcon(ImageUtils.resize(icon, iconSize.width, iconSize.height)));

            icon = ImageIO.read(new File("./assets/icons/pencil.png"));
            edit.setIcon(new ImageIcon(ImageUtils.resize(icon, iconSize.width, iconSize.height)));

            icon = ImageIO.read(new File("./assets/icons/trash.png"));
            delete.setIcon(new ImageIcon(ImageUtils.resize(icon, iconSize.width, iconSize.height)));
        } catch (IOException e) {

        }
        editToolBar.add(add);
        editToolBar.add(edit);
        editToolBar.add(delete);
        editToolBar.setOrientation(SwingConstants.HORIZONTAL);
        editToolBar.setFloatable(false);

        return editToolBar;
    }

    private class NewAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            new AddItemDialog(CatalogDialog.this, CatalogDialog.this.tableName, null, false).setVisible(true);
        }
    }

    private class EditAction implements ActionListener {
        Logger log = Logger.getLogger(EditAction.class.getName());
        @Override
        public void actionPerformed(ActionEvent e) {
            java.lang.Object obj = null;
            int id;
            String tmp;
            int selectedRow = data.getSelectedRow();
            log.debug("method: actionPerformed Selected row: " + selectedRow);
            switch (CatalogDialog.this.tableName) {
                case DatabaseSchema.TABLE_EMPLOYERS:
                    Employer employer = new Employer();
                    ResultSet resultSet = ((ResultSetTableModel) data.getModel()).getResultSet();
                    id = (Integer) data.getModel().getValueAt(selectedRow, 0);
                    tmp = (String) data.getModel().getValueAt(selectedRow, 1);
                    employer.setId(id);
                    employer.setName(tmp);
                    employer.setSurname((String) data.getModel().getValueAt(selectedRow, 2));
                    employer.setPatronymic((String) data.getModel().getValueAt(selectedRow, 3));
                    employer.setPostId((Integer) data.getModel().getValueAt(selectedRow, 5));
                    employer.setAccessLevel((Integer) data.getModel().getValueAt(selectedRow, 4));
                    log.debug("method: actionPerformed Update employer: " + employer);
                    obj = employer;
                    break;
                case DatabaseSchema.TABLE_POSTS:
                    Post post = new Post();
                    id = (Integer) data.getModel().getValueAt(selectedRow, 0);
                    tmp = (String) data.getModel().getValueAt(selectedRow, 1);
                    post.setId(id);
                    post.setName(tmp);
                    log.debug("method: actionPerformed Update post: " + post);
                    obj = post;
                    break;
                case DatabaseSchema.TABLE_PRODUCTS:
                    Product product = new Product();
                    id = (Integer) data.getModel().getValueAt(selectedRow, 1);
                    tmp = (String) data.getModel().getValueAt(selectedRow, 0);
                    product.setId(id);
                    product.setName(tmp);
                    product.setCategoryId((Integer) data.getModel().getValueAt(selectedRow, 3));
                    product.setLabel((String) data.getModel().getValueAt(selectedRow, 2));
                    product.setUnitId((Integer) data.getModel().getValueAt(selectedRow, 4));
                    obj = product;
                    log.debug("method: actionPerformed Update product: " + product);

                    break;
                case DatabaseSchema.TABLE_CATEGORIES:
                    Category category = new Category();
                    id = (Integer) data.getModel().getValueAt(selectedRow, 0);
                    tmp = (String) data.getModel().getValueAt(selectedRow, 1);
                    category.setId(id);
                    category.setName(tmp);
                    log.debug("method: actionPerformed Update category: " + category);
                    obj = category;
                    break;
                case DatabaseSchema.TABLE_UNITS:
                    Unit unit = new Unit();
                    id = (Integer) data.getModel().getValueAt(selectedRow, 0);
                    tmp = (String) data.getModel().getValueAt(selectedRow, 1);
                    unit.setId(id);
                    unit.setName(tmp);
                    unit.setShortName((String) data.getModel().getValueAt(selectedRow,2));
                    log.debug("method: actionPerformed Update unit: " + unit);
                    obj = unit;
                    break;
                case DatabaseSchema.TABLE_OBJECTS:
                    com.blagoseng.dao.model.Object object = new com.blagoseng.dao.model.Object();
                    id = (Integer) data.getModel().getValueAt(selectedRow, 0);
                    tmp = (String) data.getModel().getValueAt(selectedRow, 1);
                    object.setId(id);
                    object.setName(tmp);
                    log.debug("method: actionPerformed Update object: " + object);
                    obj = object;
                    break;
                default: /* do nothing */
            }
            if(data == null) {
                log.error("Invalid data object");
                throw new NullPointerException("Data for edit can't be null!");
            }
            new AddItemDialog(CatalogDialog.this, CatalogDialog.this.tableName, obj, true).setVisible(true);
        }
    }

    private class DeleteAction implements ActionListener {
        Logger log = Logger.getLogger(DeleteAction.class.getName());
        @Override
        public void actionPerformed(ActionEvent e) {
            int selectedRow = data.getSelectedRow();
            System.out.println("Selected row: " + selectedRow);
            log.debug("method: actionPerformed Selected row: " + selectedRow);
            if(selectedRow != -1) {
                deleteItem(selectedRow);
                loadData();
            }
        }
    }

    private JTable data;
    private String tableName;
}
