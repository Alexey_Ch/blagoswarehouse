package com.blagoseng.gui.dialogs;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.model.*;
import com.blagoseng.dao.model.Object;
import com.blagoseng.interfaces.HasName;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;
import org.painlessgridbag.PainlessGridBag;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 02.07.13
 * Time: 3:09
 */
public class AddItemDialog extends JDialog {
    private static Logger log = Logger.getLogger(AddItemDialog.class.getName());
    public AddItemDialog(JDialog parent, String tableName, java.lang.Object data, boolean updateAction) {
        super(parent, true);
        this.parent = parent;
        this.updateAction = updateAction;
        this.object = data;

        log.debug("Dialog for table - " + tableName);
        setTitle(Strings.DIALOG_CAPTION + tableName);
        this.tableName = tableName;
        createDialogUI();
        pack();
        setLocationRelativeTo(null);
    }

    private void createDialogUI() {
        acceptButton = new JButton(Strings.ADD_BUTTON);
        cancelButton = new JButton(Strings.CANCEL_BUTTON);
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddItemDialog.this.dispose();
            }
        });

        if(updateAction) {
            acceptButton.setText(Strings.UPDATE_BUTTON);
        }
        JPanel contentPanel;
        log.debug("method: createDialogUI( " + tableName + " );");
        switch (tableName) {
            case DatabaseSchema.TABLE_POSTS:
                contentPanel = createPostsPanel();
                log.debug("Content panel for " + DatabaseSchema.TABLE_POSTS + " created.");
                break;
            case  DatabaseSchema.TABLE_UNITS:
                contentPanel = createUnitsPanel();
                log.debug("Content panel for " + DatabaseSchema.TABLE_UNITS + " created.");
                break;
            case DatabaseSchema.TABLE_OBJECTS:
                contentPanel = createObjectsPanel();
                log.debug("Content panel for " + DatabaseSchema.TABLE_OBJECTS + " created.");
                break;
            case DatabaseSchema.TABLE_PRODUCTS:
                contentPanel = createProductsPanel();
                log.debug("Content panel for " + DatabaseSchema.TABLE_PRODUCTS + " created.");
                break;

            case DatabaseSchema.TABLE_CATEGORIES:
                contentPanel = createCategoriesPanel();
                log.debug("Content panel for " + DatabaseSchema.TABLE_CATEGORIES + " created.");
                break;

            case DatabaseSchema.TABLE_EMPLOYERS:
                contentPanel = createEmployersPanel();
                log.debug("Content panel for " + DatabaseSchema.TABLE_EMPLOYERS + " created.");
                break;
            default: contentPanel = new JPanel();
        }
        setContentPane(contentPanel);
    }

    private JPanel createEmployersPanel() {
        log.debug("method: createEmployersPanel");
        String[] names = DatabaseUtils.getColumnNamesForTable(DatabaseSchema.TABLE_EMPLOYERS);
        JPanel panel = new JPanel(new GridLayout(6,2));
        JLabel labelName = new JLabel(names[1]);
        final JTextField nameField = new JTextField();
        JLabel labelSurname = new JLabel(names[2]);
        final JTextField surnameField = new JTextField();
        JLabel labelPatronymic = new JLabel(names[3]);
        final JTextField patronymicField = new JTextField();
        JLabel labelAccess = new JLabel(names[4]);
        final JComboBox<Right> accessCombo = new JComboBox<>(DAOFactory.getMySQLDAOFactory().getRightDAO().getRights());
        accessCombo.setRenderer(new MyCombBoxRenderer());

        JLabel labelPost = new JLabel(names[5]);
        final JComboBox<Post> postsCombo = new JComboBox<>(DAOFactory.getMySQLDAOFactory().getPostDAO().getPosts());
        postsCombo.setRenderer(new MyCombBoxRenderer());
        if(updateAction) {
            nameField.setText(((Employer) object).getName());
            surnameField.setText(((Employer) object).getSurname());
            patronymicField.setText(((Employer) object).getPatronymic());
            postsCombo.setSelectedItem(DAOFactory.getMySQLDAOFactory().getPostDAO().getPost(((Employer) object).getPostId()));
            accessCombo.setSelectedItem(DAOFactory.getMySQLDAOFactory().getRightDAO().getRight(((Employer) object).getAccessLevel()));
        }
        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Employer newEmployer = new Employer();
                newEmployer.setName(nameField.getText().trim());
                newEmployer.setSurname(surnameField.getText().trim());
                newEmployer.setPatronymic(patronymicField.getText().trim());
                newEmployer.setPostId(((Post) postsCombo.getSelectedItem()).getId());
                newEmployer.setAccessLevel(((Right) accessCombo.getSelectedItem()).getId());
                if(!updateAction) {
                    DAOFactory.getDAOFactory(DAOFactory.MYSQL).getEmployerDAO().insertEmployer(newEmployer);
                } else {
                    newEmployer.setId(((Employer) object).getId());
                    DAOFactory.getMySQLDAOFactory().getEmployerDAO().updateEmployer(newEmployer);
                }

                try {
                    ((CatalogDialog) AddItemDialog.this.parent).loadData();
                }catch (ClassCastException ex) {
                    ex.printStackTrace();
                }
                AddItemDialog.this.dispose();
            }
        });

        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(labelName).cellXRemainder(nameField).fillX();
        gbl.row().cell(labelSurname).cellXRemainder(surnameField).fillX();
        gbl.row().cell(labelPatronymic).cellXRemainder(patronymicField).fillX();
        gbl.row().cell(labelPost).cellXRemainder(postsCombo).fillX();
        gbl.row().cell(labelAccess).cellXRemainder(accessCombo).fillX();
        gbl.row().cell(acceptButton).cell(cancelButton).fillX();

        gbl.constraints(acceptButton).weightx = 0.5;
        gbl.constraints(acceptButton).fill = GridBagConstraints.HORIZONTAL;

        gbl.constraints(cancelButton).weightx = 0.5;

        gbl.doneAndPushEverythingToTop();
        return panel;
    }

    private JPanel createCategoriesPanel() {
        log.debug("method: createCategoriesPanel");
        JPanel panel = new JPanel(new GridLayout(2,2));
        JLabel labelPost = new JLabel(DatabaseSchema.CATEGORIES.CATEGORIES_COLUMN_NAMES[0]);
        final JTextField textField = new JTextField();
        if(updateAction) {
            textField.setText(((Category) object).getName());
        }
        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!updateAction) {
                    DAOFactory.getDAOFactory(DAOFactory.MYSQL).
                            getCategoryDAO().insertCategory(textField.getText().trim());
                }else {
                    Category category = new Category();
                    category.setId(((Category) object).getId());
                    category.setName(textField.getText().trim());
                    DAOFactory.getMySQLDAOFactory().getCategoryDAO().updateCategory(category);
                }
                try {
                    ((CatalogDialog) AddItemDialog.this.parent).loadData();
                }catch (ClassCastException ex) {
                    ex.printStackTrace();
                }
                AddItemDialog.this.dispose();
            }
        });

        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(labelPost).cellXRemainder(textField).fillX();
        gbl.row().cell(acceptButton).cell(cancelButton).fillX();

        gbl.constraints(acceptButton).weightx = 0.5;
        gbl.constraints(acceptButton).fill = GridBagConstraints.HORIZONTAL;

        gbl.constraints(cancelButton).weightx = 0.5;

        gbl.doneAndPushEverythingToTop();

        /*panel.add(labelPost);
        panel.add(textField);
        panel.add(acceptButton);
        panel.add(cancelButton);*/
        return panel;
    }

    private JPanel createProductsPanel() {
        log.debug("method: createProductsPanel");
        String[] names = DatabaseUtils.getColumnNamesForTable(DatabaseSchema.TABLE_PRODUCTS);
        JPanel panel = new JPanel(new GridLayout(6,2));
        // Name
        JLabel labelName = new JLabel(names[0]);
        final JTextField nameField = new JTextField();
        JLabel labelLabel = new JLabel(names[2]);
        final JTextField labelField = new JTextField();

        JLabel labelCategory = new JLabel(names[3]);
        List<Category> categoryList = DAOFactory.getMySQLDAOFactory().getCategoryDAO().getAllCategories();
        final JComboBox<Category> categoryCombo = new JComboBox<>(categoryList.toArray(new Category[categoryList.size()]));
        categoryCombo.setRenderer(new MyCombBoxRenderer());

        JLabel labelUnit = new JLabel(names[4]);
        final JComboBox<Unit> unitCombo = new JComboBox<>(DAOFactory.getMySQLDAOFactory().getUnitDAO().getUnits());
        unitCombo.setRenderer(new MyCombBoxRenderer());
        if(updateAction) {
            nameField.setText(((Product) object).getName());
            labelField.setText(((Product) object).getLabel());
            categoryCombo.setSelectedItem(DAOFactory.getMySQLDAOFactory()
                    .getCategoryDAO().findCategory(((Product) object).getCategoryId()));
            unitCombo.setSelectedItem(DAOFactory.getMySQLDAOFactory()
                    .getUnitDAO().getUnit(((Product) object).getUnitId()));
        }
        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product product = new Product();
                product.setName(nameField.getText().trim());
                product.setLabel(labelField.getText().trim());
                product.setCategoryId(((Category) categoryCombo.getSelectedItem()).getId());
                product.setUnitId(((Unit) unitCombo.getSelectedItem()).getId());
                if(!updateAction) {
                    DAOFactory.getDAOFactory(DAOFactory.MYSQL).getProductDAO().insertProduct(product);
                } else {
                    product.setId(((Product) object).getId());
                    DAOFactory.getMySQLDAOFactory().getProductDAO().updateProduct(product);
                }

                /*try {
                    ((SelectItemDialog) AddItemDialog.this.parent).loadData();
                }catch (ClassCastException ex) {
                    ex.printStackTrace();
                }*/
                AddItemDialog.this.dispose();
            }
        });

        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(labelName).cellXRemainder(nameField).fillX();
        gbl.row().cell(labelLabel).cellXRemainder(labelField).fillX();
        gbl.row().cell(labelCategory).cellXRemainder(categoryCombo).fillX();
        gbl.row().cell(labelUnit).cellXRemainder(unitCombo).fillX();
        gbl.row().cell(acceptButton).cell(cancelButton).fillX();

        gbl.constraints(acceptButton).weightx = 0.5;
        gbl.constraints(acceptButton).fill = GridBagConstraints.HORIZONTAL;

        gbl.constraints(cancelButton).weightx = 0.5;

        gbl.doneAndPushEverythingToTop();


       /* panel.add(labelName);
        panel.add(nameField);
        panel.add(labelLabel);
        panel.add(labelField);
        panel.add(labelCategory);
        panel.add(categoryCombo);

        panel.add(labelUnit);
        panel.add(unitCombo);

        panel.add(acceptButton);
        panel.add(cancelButton);*/
        return panel;
    }

    private JPanel createPostsPanel() {
        log.debug("method: createPostsPanel");
        JPanel panel = new JPanel(new GridLayout(2,2));
        JLabel labelPost = new JLabel(DatabaseSchema.POSTS_COLUMN_NAMES[0]);
        final JTextField textField = new JTextField();
        if(updateAction) {
            textField.setText(((Post) object).getName());
        }
        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!updateAction) {
                    DAOFactory.getDAOFactory(DAOFactory.MYSQL).getPostDAO().insertPost(textField.getText().trim());
                } else {
                    Post post = (Post) object;
                    String old = post.getName();
                    post.setName(textField.getText().trim());
                    log.debug("Post will be update from " + old + " to " + post.getName());
                    DAOFactory.getDAOFactory(DAOFactory.MYSQL).getPostDAO().updatePost(post);
                }
                try {
                    ((CatalogDialog) AddItemDialog.this.parent).loadData();
                }catch (ClassCastException ex) {
                    ex.printStackTrace();
                }
                AddItemDialog.this.dispose();
            }
        });

        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(labelPost).cellXRemainder(textField).fillX();
        gbl.row().cell(acceptButton).cell(cancelButton).fillX();

        gbl.constraints(acceptButton).weightx = 0.5;
        gbl.constraints(acceptButton).fill = GridBagConstraints.HORIZONTAL;

        gbl.constraints(cancelButton).weightx = 0.5;

        gbl.doneAndPushEverythingToTop();

        /*panel.add(labelPost);
        panel.add(textField);
        panel.add(acceptButton);
        panel.add(cancelButton);*/
        return panel;
    }

    private JPanel createUnitsPanel() {
        log.debug("method: createUnitsPanel");
        JPanel panel = new JPanel(new GridLayout(3,2));
        JLabel labelPost = new JLabel(DatabaseSchema.UNITS_COLUMN_NAMES[1]);
        final JTextField textField = new JTextField();
        JLabel labelShort = new JLabel(DatabaseSchema.UNITS_COLUMN_NAMES[2]);
        final JTextField shortName = new JTextField();
        if(updateAction) {
            textField.setText(((Unit) object).getName());
            shortName.setText(((Unit) object).getShortName());
        }
        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Unit unit = new Unit();
                unit.setName(textField.getText().trim());
                unit.setShortName(shortName.getText().trim());
                if(!updateAction) {
                    DAOFactory.getDAOFactory(DAOFactory.MYSQL).getUnitDAO().insertUnit(unit);
                } else {
                    unit.setId(((Unit) object).getId());
                    DAOFactory.getMySQLDAOFactory().getUnitDAO().updateUnit(unit);
                }
                try {
                    ((CatalogDialog) AddItemDialog.this.parent).loadData();
                }catch (ClassCastException ex) {
                    ex.printStackTrace();
                }
                AddItemDialog.this.dispose();
            }
        });

        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(labelPost).cellXRemainder(textField).fillX();
        gbl.row().cell(labelShort).cellXRemainder(shortName).fillX();
        gbl.row().cell(acceptButton).cell(cancelButton).fillX();

        gbl.constraints(acceptButton).weightx = 0.5;
        gbl.constraints(acceptButton).fill = GridBagConstraints.HORIZONTAL;

        gbl.constraints(cancelButton).weightx = 0.5;

        gbl.doneAndPushEverythingToTop();

        /*panel.add(labelPost);
        panel.add(textField);
        panel.add(labelShort);
        panel.add(shortName);
        panel.add(acceptButton);
        panel.add(cancelButton);*/
        return panel;
    }

    private JPanel createObjectsPanel() {
        log.debug("method: createObjectsPanel");
        JPanel panel = new JPanel(new GridLayout(2,2));
        JLabel labelPost = new JLabel(DatabaseSchema.OBJECTS_COLUMN_NAMES[0]);
        final JTextField textField = new JTextField();
        if(updateAction) {
            textField.setText(((Object) object).getName());
        }
        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object o = new Object();
                o.setName(textField.getText().trim());
                if(!updateAction) {
                    DAOFactory.getDAOFactory(DAOFactory.MYSQL).getObjectDAO().createObject(o);
                } else {
                    o.setId(((Object) object).getId());
                    DAOFactory.getMySQLDAOFactory().getObjectDAO().updateObject(o);
                }
                try {
                    ((CatalogDialog) AddItemDialog.this.parent).loadData();
                }catch (ClassCastException ex) {
                    ex.printStackTrace();
                }
                AddItemDialog.this.dispose();
            }
        });

        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(labelPost).cellXRemainder(textField).fillX();
        gbl.row().cell(acceptButton).cell(cancelButton).fillX();

        gbl.constraints(acceptButton).weightx = 0.5;
        gbl.constraints(acceptButton).fill = GridBagConstraints.HORIZONTAL;

        gbl.constraints(cancelButton).weightx = 0.5;

        gbl.doneAndPushEverythingToTop();

        /*panel.add(labelPost);
        panel.add(textField);
        panel.add(acceptButton);
        panel.add(cancelButton);*/
        return panel;
    }

    public static class MyCombBoxRenderer extends JLabel implements ListCellRenderer<HasName> {
        public MyCombBoxRenderer() {
            setOpaque(true);
            setHorizontalAlignment(CENTER);
            setVerticalAlignment(CENTER);
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends HasName> list, HasName value, int index, boolean isSelected, boolean cellHasFocus) {

                if (isSelected) {
                    setBackground(list.getSelectionBackground());
                    setForeground(list.getSelectionForeground());
                } else {
                    setBackground(list.getBackground());
                    setForeground(list.getForeground());
                }

                setText(value.getName());
                return this;
        }
    }
    private JButton acceptButton;
    private JButton cancelButton;
    private String tableName;
    private JDialog parent;
    private java.lang.Object object;
    private boolean updateAction;
}
