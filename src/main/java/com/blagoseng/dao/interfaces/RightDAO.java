package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.Right;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 03.07.13
 * Time: 12:57
 */
public interface RightDAO {
    public Right[] getRights();
    public Right getRight(int rightId);
}
