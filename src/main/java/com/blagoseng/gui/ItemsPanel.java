package com.blagoseng.gui;

import com.blagoseng.gui.dialogs.RemainderDescription;
import com.blagoseng.gui.models.ProductsRemainderTableModel;
import com.blagoseng.gui.panels.FilterPanel;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 04.07.13
 * Time: 10:49
 */
public class ItemsPanel extends JPanel {
    private static final Logger LOG = Logger.getLogger(ItemsPanel.class);

    public ItemsPanel() {
        this.document = document;
        createGUI();
    }
    private void createGUI() {
        setLayout(new BorderLayout());
        add(createFilterPanel(), BorderLayout.NORTH);
        add(createTablePanel(), BorderLayout.CENTER);
    }

    private JPanel createFilterPanel() {
        panel = new FilterPanel();
        return panel;
    }

    private JPanel createTablePanel() {
        document =  panel.getDocument();
        JPanel panel = new JPanel(new BorderLayout());
        tableData = new JTable();
        ProductsRemainderTableModel model = new ProductsRemainderTableModel();
        tableData.setModel(model);
        final TableRowSorter<ProductsRemainderTableModel> sorter = new TableRowSorter<>(model);
        tableData.setRowSorter(sorter);
        document.addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {

                if (document.getLength() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    try {
                        sorter.setRowFilter(RowFilter.regexFilter(document.getText(0, document.getLength()).toString()));
                    } catch (BadLocationException el) {
                        el.printStackTrace();
                    }
                }

            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (document.getLength() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    try {
                        sorter.setRowFilter(RowFilter.regexFilter(document.getText(0, document.getLength()).toString()));
                    } catch (BadLocationException el) {
                        el.printStackTrace();
                    }
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (document.getLength() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    try {
                        sorter.setRowFilter(RowFilter.regexFilter(document.getText(0, document.getLength()).toString()));
                    } catch (BadLocationException el) {
                        el.printStackTrace();
                    }
                }
            }
        });
        tableData.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if(e.getClickCount() == 2 && !e.isConsumed()) {
                    e.consume();
                    int row = tableData.rowAtPoint(e.getPoint());
                    LOG.debug("method: mousePressed row = " + row);
                    if(row >= 0) {
                        ProductsRemainderTableModel model = (ProductsRemainderTableModel) tableData.getModel();
                        int protductId = model.getProductId(row);
                        new RemainderDescription(protductId, tableData).setVisible(true);
                    }

                }
            }});
        JScrollPane scrollPane = new JScrollPane(tableData);
        panel.add(scrollPane);
        panel.setBorder(BorderFactory.createTitledBorder("Остатки по складу"));
        return panel;
    }

    public JTable getTableData() {
        return tableData;
    }

    private JTable tableData;
    private Document document;
    private FilterPanel panel;
}
