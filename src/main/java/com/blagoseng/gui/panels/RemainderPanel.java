package com.blagoseng.gui.panels;

import com.blagoseng.gui.models.ProductsRemainderTableModel;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 10.07.13
 * Time: 4:13
 */
public class RemainderPanel extends JPanel {
    public RemainderPanel() {

    }

    private void createGui() {
        setLayout(new BorderLayout());
        data = new JTable();
        data.setModel(new ProductsRemainderTableModel());
        add(data);

    }
    JTable data;
}
