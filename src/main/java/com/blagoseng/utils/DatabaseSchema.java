package com.blagoseng.utils;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 09.06.13
 * Time: 19:13
 */
public final class DatabaseSchema {
    public static final String TABLE_UNITS = "Units";
    public static final String TABLE_EMPLOYERS = "Employe";
    public static final String TABLE_PRODUCTS = "Items";
    public static final String TABLE_OBJECTS = "Objects";
    public static final String TABLE_POSTS = "Posts";
    public static final String TABLE_CATEGORIES = "Categories";
    public static final String TABLE_RIGHTS = "Rights";
    public static final String TABLE_LISTS = "Lists";
    public static final String TABLE_INPUT_BILL = "InputBill";

    public static final String[] UNITS_COLUMN_NAMES = {"Индекс", "Название", "Сокращение"};
    public static final String[] EMPLOYERS_COLUMN_NAMES = {"Индекс","Имя", "Фамилия", "Отчество", "Уровень доступа",
                                                            "Должность"};

    public static final String[] OBJECTS_COLUMN_NAMES = {"Индекс","Объект"};
    public static final String[] POSTS_COLUMN_NAMES = {"Индекс","Должность"};

    public static final class CATEGORIES {
        public static final String TABLE_TITLE = "Категории";
        public static final String[] CATEGORIES_COLUMN_NAMES = {"Индекс","Категория"};
        public static final String F_ID = "category_id";
        public static final String F_NAME = "category_name";

    }

    public static final class PRODUCTS {
        public static final String TABLE_TITLE = "Товары";
        public static final String[] PRODUCTS_COLUMN_NAMES = {"Товар", "Индекс", "Метка", "Категория", "Ед. изм."};
        public static final String F_ID = "item_id";
        public static final String F_NAME = "name";
        public static final String F_LABEL = "label";
        public static final String F_CATEGORY_ID = "ctegory_id";
        public static final String F_UNIT_ID = "unit_id";
    }

    public static final class POSTS {
        public static final String TABLE_TITLE = "Должности";
        public static final String[] POSTS_COLUMN_NAMES = {"Индекс","Должность"};
        public static final String F_ID = "post_id";
        public static final String F_NAME = "post_name";
    }

    public static final class UNITS {
        public static final String TABLE_TITLE = "Единицы измерения";
        public static final String[] UNITS_COLUMN_NAMES = {};
        public static final String F_ID = "unit_id";
        public static final String F_NAME = "name";
        public static final String F_SHORT_NAME = "short_name";
    }

    public static final class OBJECTS {
        public static final String TABLE_TITLE = "Объекты";
        public static final String F_ID = "object_id";
        public static final String F_NAME = "object_name";
    }

    public static final class EMPLOYERS {
        public static final String TABLE_TITLE = "Сотрудники";
        public static final String F_ID = "employe_id";
        public static final String F_NAME = "name";
        public static final String F_SURNAME = "surname";
        public static final String F_PATRONYMIC = "patronymic";
        public static final String F_ACCESS_LEVEL = "access_level";
        public static final String F_POST_ID = "post_id";
    }

    public static final class RIGHTS {
        public static final String TABLE_TITLE = "Права";
        public static final String F_ID = "right_id";
        public static final String F_NAME = "level_name";
        public static final String F_DESCRIPTION = "description";
    }

    public static final class InputBill {
        public static final String TABLE_TITLE = "Приходные накладные";
        public static final String F_ID = "bill_id";
        public static final String F_DATE = "date";
        public static final String F_DESTINATION = "destination";
    }

    public static final class Lists {
        public static final String F_ID = "list_id";
        public static final String F_BILL_ID = "bill_id";
        public static final String F_ITEM_ID = "item_id";
        public static final String F_AMOUNT = "count";
    }

    private DatabaseSchema() { /* Deny to create instance */ }
}
