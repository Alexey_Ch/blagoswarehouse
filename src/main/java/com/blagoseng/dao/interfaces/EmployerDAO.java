package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.Employer;

import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 12:52
 */
public interface EmployerDAO {
    public ResultSet selectEmployerResultSet();
    public void insertEmployer(Employer employer);
    public boolean deleteEmployer(int employerId);
    boolean updateEmployer(Employer employer);
}
