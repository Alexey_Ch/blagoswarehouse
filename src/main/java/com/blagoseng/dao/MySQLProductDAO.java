package com.blagoseng.dao;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.interfaces.ProductDAO;
import com.blagoseng.dao.model.Product;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:19
 */
public class MySQLProductDAO implements ProductDAO {
    private static final Logger LOG = Logger.getLogger(MySQLProductDAO.class.getName());
    @Override
    public ResultSet selectProductResultSet() {
        String query = Strings.SQL.SELECT
                + Strings.SQL.ALL
                + Strings.SQL.FROM
                + DatabaseSchema.TABLE_PRODUCTS
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: selectProductResultSet");
        return DatabaseUtils.executeQuery(query);
    }

    @Override
    public boolean insertProduct(Product product) {
        String query = Strings.SQL.INSERT
                + DatabaseSchema.TABLE_PRODUCTS
                + Strings.SQL.START_BRACKET
                + DatabaseSchema.PRODUCTS.F_NAME
                + Strings.SQL.COMMA
                + DatabaseSchema.PRODUCTS.F_LABEL
                + Strings.SQL.COMMA
                + DatabaseSchema.PRODUCTS.F_CATEGORY_ID
                + Strings.SQL.COMMA
                + DatabaseSchema.PRODUCTS.F_UNIT_ID
                + Strings.SQL.END_BRACKET
                + Strings.SQL.VALUES
                + Strings.SQL.BACK_QUOTE
                + product.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA
                + Strings.SQL.BACK_QUOTE
                + product.getLabel()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA
                + product.getCategoryId()
                + Strings.SQL.COMMA
                + product.getUnitId()
                + Strings.SQL.END_BRACKET
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: insertProduct product: " + product);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean deleteProduct(int productId) {
        String query = Strings.SQL.DELETE
                + DatabaseSchema.TABLE_PRODUCTS
                + Strings.SQL.WHERE
                + DatabaseSchema.PRODUCTS.F_ID
                + Strings.SQL.IS
                + productId
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: deleteProduct productId: " + productId);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean updateProduct(Product product) {
        String query = Strings.SQL.UPDATE
                + DatabaseSchema.TABLE_PRODUCTS
                + Strings.SQL.SET
                + DatabaseSchema.PRODUCTS.F_NAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + product.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA
                + DatabaseSchema.PRODUCTS.F_LABEL
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + product.getLabel()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA
                + DatabaseSchema.PRODUCTS.F_CATEGORY_ID
                + Strings.SQL.IS
                + product.getCategoryId()
                + Strings.SQL.COMMA
                + DatabaseSchema.PRODUCTS.F_UNIT_ID
                + Strings.SQL.IS
                + product.getUnitId()
                + Strings.SQL.WHERE
                + DatabaseSchema.PRODUCTS.F_ID
                + Strings.SQL.IS
                + product.getId();
        LOG.debug("method: updateProduct Product: " + product);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public List<Product> getProductList() {
        String query = "SELECT * FROM " + DatabaseSchema.TABLE_PRODUCTS + ";";
        List<Product> products = new ArrayList<>();
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        Product product;

        try {
            resultSet.beforeFirst();
            while (resultSet.next()) {
                product = new Product();
                product.setId(resultSet.getInt(DatabaseSchema.PRODUCTS.F_ID));
                product.setName(resultSet.getString(DatabaseSchema.PRODUCTS.F_NAME));
                product.setLabel(resultSet.getString(DatabaseSchema.PRODUCTS.F_LABEL));
                product.setCategoryId(resultSet.getInt(DatabaseSchema.PRODUCTS.F_CATEGORY_ID));
                product.setUnitId(resultSet.getInt(DatabaseSchema.PRODUCTS.F_UNIT_ID));
                LOG.debug("method getProductList : to list Product: " + product);
                products.add(product);
            }

        }catch (SQLException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }


        return products;
    }

    @Override
    public Product getProduct(int productId) {
        Product product = null;
        String query = "Select * from " + DatabaseSchema.TABLE_PRODUCTS
                +  " where "
                + DatabaseSchema.PRODUCTS.F_ID + "="
                + productId + ";";
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        try {
            resultSet.beforeFirst();
            if(resultSet.next()) {
                product = new Product();
                product.setId(productId);
                product.setLabel(resultSet.getString(DatabaseSchema.PRODUCTS.F_LABEL));
                product.setName(resultSet.getString(DatabaseSchema.PRODUCTS.F_NAME));
                product.setUnitId(resultSet.getInt(DatabaseSchema.PRODUCTS.F_UNIT_ID));
                product.setCategoryId(resultSet.getInt(DatabaseSchema.PRODUCTS.F_CATEGORY_ID));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return  product;
    }
}