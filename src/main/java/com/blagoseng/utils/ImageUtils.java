package com.blagoseng.utils;

import com.blagoseng.constants.dimensions.Dimensions;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 1:55
 */
public final class ImageUtils {
    private static final Logger LOG = Logger.getLogger(ImageUtils.class);
    public static BufferedImage resize(BufferedImage image, int width, int height) {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
        Graphics2D g2d = bi.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(image, 0, 0, width, height, null);
        g2d.dispose();
        return bi;
    }

    public static JButton addButtonIcon(JButton button, Dimension imageDimension, String imagePath) {
        try {
            Dimension iconDim = imageDimension;
            BufferedImage bufImage = ImageIO.read(new File(imagePath));
            button.setIcon(new ImageIcon(ImageUtils.resize(bufImage, iconDim.width, iconDim.height)));
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return button;
    }
}
