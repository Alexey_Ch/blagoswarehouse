package com.blagoseng.utils;

import com.blagoseng.dao.model.Category;
import com.blagoseng.dao.model.Unit;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 12.07.13
 * Time: 1:17
 */
public final class ListUtils {
    public static Unit getUnitById(List<Unit> units, int id) {
        for(Unit p : units) {
            if(p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    public static Category getCategoryById(List<Category> categories, int id) {
        for(Category p : categories) {
            if(p.getId() == id) {
                return p;
            }
        }
        return null;
    }
}
