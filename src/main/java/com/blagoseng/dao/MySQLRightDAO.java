package com.blagoseng.dao;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.interfaces.RightDAO;
import com.blagoseng.dao.model.Post;
import com.blagoseng.dao.model.Right;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 03.07.13
 * Time: 12:59
 */
public class MySQLRightDAO implements RightDAO{
    private static Logger log = Logger.getLogger(MySQLRightDAO.class.getName());

    @Override
    public Right[] getRights() {
        List<Right> rightList = new ArrayList<Right>();
        String query = Strings.SQL.SELECT +
                Strings.SQL.ALL +
                Strings.SQL.FROM +
                DatabaseSchema.TABLE_RIGHTS +
                Strings.SQL.SEMICOLON;
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        log.debug("method: getRights");
        Right right;

        try {
            resultSet.beforeFirst();
            resultSet.next();
            while (!resultSet.isAfterLast()) {
                right = new Right();
                right.setId(resultSet.getInt(DatabaseSchema.RIGHTS.F_ID));
                right.setName(resultSet.getString(DatabaseSchema.RIGHTS.F_NAME));
                right.setDescription(resultSet.getString(DatabaseSchema.RIGHTS.F_DESCRIPTION));
                rightList.add(right);
                resultSet.next();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }

        return rightList.toArray(new Right[rightList.size()]);
    }

    // Return null if no Right record with selected id.
    public Right getRight(int rightId) {
        String query = Strings.SQL.SELECT
                + Strings.SQL.ALL
                + Strings.SQL.FROM
                + DatabaseSchema.TABLE_RIGHTS
                + Strings.SQL.WHERE
                + DatabaseSchema.RIGHTS.F_ID
                + Strings.SQL.IS
                + rightId;
        log.debug("method: gerRight rightId: " + rightId);
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        Right right = null;
        try {
            resultSet.beforeFirst();
            if(resultSet.next()) {
                right = new Right();
                right.setId(rightId);
                right.setName(resultSet.getString(DatabaseSchema.RIGHTS.F_NAME));
                right.setDescription(resultSet.getString(DatabaseSchema.RIGHTS.F_DESCRIPTION));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return right;
    }
}
