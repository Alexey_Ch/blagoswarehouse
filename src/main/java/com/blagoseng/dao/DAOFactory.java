package com.blagoseng.dao;

import com.blagoseng.dao.interfaces.*;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 12:51
 */
public abstract class DAOFactory {
    public static final int MYSQL = 0;

    public abstract CategoryDAO getCategoryDAO();
    public abstract EmployerDAO getEmployerDAO();
    public abstract OutBillDAO getOutBillDAO();
    public abstract ObjectDAO getObjectDAO();
    public abstract PostDAO getPostDAO();
    public abstract ProductDAO getProductDAO();
    public abstract UnitDAO getUnitDAO();
    public abstract RightDAO getRightDAO();
    public abstract ListDAO getListDAO();
    public abstract InputBillDAO getInputBillDAO();
    public abstract RemainderDescriptionDAO getRemainderDescriptionDAO();

    public static DAOFactory getDAOFactory (int whichFactory) {
        switch (whichFactory) {
            case MYSQL:
                return mySQLDAOFactory;
            default:
                return null;
        }
    }

    public static MySQLDAOFactory getMySQLDAOFactory() {
        return (MySQLDAOFactory)getDAOFactory(DAOFactory.MYSQL);
    }

    private static MySQLDAOFactory mySQLDAOFactory = new MySQLDAOFactory();
}
