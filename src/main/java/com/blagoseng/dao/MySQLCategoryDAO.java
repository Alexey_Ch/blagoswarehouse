package com.blagoseng.dao;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.interfaces.CategoryDAO;
import com.blagoseng.dao.model.Category;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:04
 */
public class MySQLCategoryDAO implements CategoryDAO {
    private static final Logger LOG = Logger.getLogger(MySQLCategoryDAO.class.getName());

    @Override
    public boolean insertCategory(Category category) {
        return insertCategory(category.getName());
    }

    @Override
    public boolean insertCategory(String categoryName) {
        String query = Strings.SQL.INSERT
                + DatabaseSchema.TABLE_CATEGORIES
                + Strings.SQL.START_BRACKET
                + DatabaseSchema.CATEGORIES.F_NAME
                + Strings.SQL.END_BRACKET
                + Strings.SQL.VALUES
                + Strings.SQL.BACK_QUOTE
                + categoryName
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.END_BRACKET
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: insertCategory categoryName: " + categoryName);

        //If category with the same name already exits in database
        if(findCategory(categoryName) != null) {
            //Return false insertion because record with the same name already exist
            LOG.debug("method: insertCategory result: " +
                    "Return false insertion because record with the same name already exist");
            return false;
        }

        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public List<Category> getAllCategories() {
        Category category = null;
        ResultSet resultSet = DatabaseUtils.selectAllFromTable(DatabaseSchema.TABLE_CATEGORIES);
        List<Category> categories = new ArrayList<Category>();
        if(resultSet != null) {
            try{
                resultSet.beforeFirst();
                while (resultSet.next()) {
                    category = createCategoryFromResultSet(resultSet);
                    if(category != null) {
                       categories.add(category);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
            }
        }
        return categories;
    }

    @Override
    public Category findCategory(String categoryName) {
        Category category = null;
        Connection conn = DAOFactory.getMySQLDAOFactory().getConnection();
        try{
            Statement stat = conn.createStatement();
            List<String> fields = new ArrayList<String>();
            fields.add(Strings.SQL.ALL);
            List<String> table = new ArrayList<String>();
            table.add(DatabaseSchema.TABLE_CATEGORIES);
            String tale = Strings.SQL.WHERE
                    + DatabaseSchema.CATEGORIES.F_NAME
                    + Strings.SQL.IS
                    + Strings.SQL.BACK_QUOTE
                    + categoryName
                    + Strings.SQL.BACK_QUOTE;
            ResultSet resultSet = stat.executeQuery(DatabaseUtils.createSelectQuery(fields, table, tale));
            resultSet.beforeFirst();
            if(resultSet.next()) {
                category = createCategoryFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return category;
    }

    @Override
    public Category findCategory(int categoryId) {
        Category category = null;
        Connection conn = DAOFactory.getMySQLDAOFactory().getConnection();
        try{
            Statement stat = conn.createStatement();
            List<String> fields = new ArrayList<String>();
            fields.add(Strings.SQL.ALL);
            List<String> table = new ArrayList<String>();
            table.add(DatabaseSchema.TABLE_CATEGORIES);
            String tale = Strings.SQL.WHERE
                    + DatabaseSchema.CATEGORIES.F_ID
                    + Strings.SQL.IS + categoryId
                    + Strings.SQL.SEMICOLON;
            ResultSet resultSet = stat.executeQuery(DatabaseUtils.createSelectQuery(fields, table, tale));
            resultSet.beforeFirst();
            if(resultSet.next()) {
                category = createCategoryFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return category;
    }

    public ResultSet getCategoryResultSet() {
        String query = Strings.SQL.SELECT
                + Strings.SQL.ALL
                + Strings.SQL.FROM
                + DatabaseSchema.TABLE_CATEGORIES
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: getCategoryResultSet");
        return DatabaseUtils.executeQuery(query);
    }

    @Override
    public Category getCategory(int categoryId) {
        String query = "SELECT * FROM " + DatabaseSchema.TABLE_CATEGORIES +
                " WHERE " + DatabaseSchema.CATEGORIES.F_ID +
                "=" + categoryId + ";";
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        Category category = null;
        try {
            resultSet.beforeFirst();
            if(resultSet.next()) {
                category = new Category();
                category.setId(resultSet.getInt(DatabaseSchema.CATEGORIES.F_ID));
                category.setName(resultSet.getString(DatabaseSchema.CATEGORIES.F_NAME));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return category;
    }

    @Override
    public boolean deleteCategory(String categoryName) {
        String query = Strings.SQL.DELETE
                + DatabaseSchema.TABLE_CATEGORIES
                + Strings.SQL.WHERE
                + DatabaseSchema.CATEGORIES.F_NAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + categoryName
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: deleteCategory categoryName: " + categoryName);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean deleteCategory(int categoryId) {
        String query = Strings.SQL.DELETE
                + DatabaseSchema.TABLE_CATEGORIES
                + Strings.SQL.WHERE
                + DatabaseSchema.CATEGORIES.F_ID
                + Strings.SQL.IS
                + categoryId
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: deleteCategory categoryId: " + categoryId);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean updateCategory(Category category) {
        String query = Strings.SQL.UPDATE
                + DatabaseSchema.TABLE_CATEGORIES
                + Strings.SQL.SET
                + DatabaseSchema.CATEGORIES.F_NAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + category.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.WHERE
                + DatabaseSchema.CATEGORIES.F_ID
                + Strings.SQL.IS
                + category.getId();
        LOG.debug("method: updateCategory Category: " + category);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    private Category createCategoryFromResultSet(ResultSet resultSet) {
        Category category = new Category();
        try {
            category.setId(resultSet.getInt(DatabaseSchema.CATEGORIES.F_ID));
            category.setName(resultSet.getString(DatabaseSchema.CATEGORIES.F_NAME));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return category;
    }
}
