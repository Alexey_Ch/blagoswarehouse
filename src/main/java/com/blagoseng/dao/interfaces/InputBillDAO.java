package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.InputBill;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 13:07
 */
public interface InputBillDAO {
    public int createInputBill(InputBill inputBill);
    public boolean updateInputBill(InputBill bill);
    public boolean deleteInputBill(int inputBillId);
    public InputBill getInputBill(int inputBillId);
    public List<InputBill> getInputBills();
}
