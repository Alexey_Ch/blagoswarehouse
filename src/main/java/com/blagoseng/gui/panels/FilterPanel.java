package com.blagoseng.gui.panels;

import org.painlessgridbag.PainlessGridBag;

import javax.swing.*;
import javax.swing.text.Document;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 09.07.13
 * Time: 9:49
 */
public class FilterPanel extends JPanel {
    public FilterPanel() {
        createFilterPanel();
    }

    private void createFilterPanel() {
        JCheckBox item = new JCheckBox("Товар");
        JCheckBox category = new JCheckBox("Категория");
        JTextField text = new JTextField();
        document = text.getDocument();
        JLabel search = new JLabel("Поиск: ");
        JCheckBox labelText = new JCheckBox("Метка");
        boolean debug =  false;
        PainlessGridBag gbl = new PainlessGridBag(this, debug);
        gbl.row().cell(search).cellX(text, 3).fillX();
        gbl.row().cell(item).cell(category).cell(labelText).fillX();


        gbl.constraints(search).gridx = 0;
        gbl.constraints(text).gridx = 1;
        gbl.constraints(text).insets.left = 10;

        gbl.constraints(labelText).gridx = 3;
        gbl.constraints(category).gridx = 2;
        gbl.constraints(item).gridx = 1;

        gbl.doneAndPushEverythingToTop();
        setBorder(BorderFactory.createTitledBorder("Фильтр: "));
    }

    public Document getDocument() {
        return document;
    }

    private Document document;
}
