package com.blagoseng.dao;

import com.blagoseng.dao.interfaces.RemainderDescriptionDAO;
import com.blagoseng.dao.model.RemainderDescription;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 11.07.13
 * Time: 22:32
 */
public class MySQLRemainderDescriptionDAO implements RemainderDescriptionDAO {
    private static final Logger LOG = Logger.getLogger(MySQLRemainderDescriptionDAO.class);

    @Override
    public List<RemainderDescription> getRemainderDescriptionList(int itemId) {
        List<RemainderDescription> list = new ArrayList<>();
        String query = "SELECT destination, item_id, SUM(count) AS sum " +
                "FROM InputBill AS I, Lists AS L " +
                "WHERE I.bill_id = L.bill_id AND item_id=" + itemId + " " +
                "GROUP BY destination;";

        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        try {
            resultSet.beforeFirst();
            RemainderDescription remainder;
            while (resultSet.next()) {
                remainder = new RemainderDescription();
                remainder.setDestination(resultSet.getInt("destination"));
                remainder.setItemId(resultSet.getInt("item_id"));
                remainder.setAmount(resultSet.getInt("sum"));
                LOG.debug("method getRemainderDescriptionList New REM DESCR: " + remainder + "\n");
                list.add(remainder);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
