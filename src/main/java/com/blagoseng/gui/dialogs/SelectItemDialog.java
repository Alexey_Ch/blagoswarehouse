package com.blagoseng.gui.dialogs;

import com.blagoseng.constants.dimensions.Dimensions;
import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.model.Lists;
import com.blagoseng.dao.model.Product;
import com.blagoseng.gui.models.ProductTableModel;
import com.blagoseng.gui.models.ProductsRemainderTableModel;
import com.blagoseng.gui.models.ResultSetTableModel;
import com.blagoseng.gui.panels.FilterPanel;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import com.blagoseng.utils.ImageUtils;
import org.apache.log4j.Logger;
import org.painlessgridbag.PainlessGridBag;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 04.07.13
 * Time: 11:09
 */
public class SelectItemDialog extends JDialog {
    private static final Logger LOG = Logger.getLogger(SelectItemDialog.class);

    public SelectItemDialog(Window parent) {
        super(parent, "Добавить товар", ModalityType.APPLICATION_MODAL);
        this.parent = parent;
        add(createMainPanel(), BorderLayout.CENTER);
        setPreferredSize(new Dimension(700, 600));
        pack();
        setLocationRelativeTo(null);
    }

    private JPanel createTablePanel() {
        JPanel panel = new JPanel(new BorderLayout());
        data = new JTable();
        /*ResultSetTableModel model = new ResultSetTableModel(DAOFactory.getMySQLDAOFactory().getProductDAO().selectProductResultSet(),
                DatabaseUtils.getColumnNamesForTable(DatabaseSchema.TABLE_PRODUCTS));
        data.setModel(model);
        final TableRowSorter<ResultSetTableModel> sorter = new TableRowSorter<>(model);*/
        ProductTableModel model = new ProductTableModel();
        data.setModel(model);
        final TableRowSorter<ProductTableModel> sorter = new TableRowSorter<>(model);
        document = filterPanel.getDocument();
        data.setRowSorter(sorter);
        document.addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {

                if (document.getLength() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    try {
                        sorter.setRowFilter(RowFilter.regexFilter(document.getText(0, document.getLength()).toString()));
                    } catch (BadLocationException el) {
                        el.printStackTrace();
                    }
                }

            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (document.getLength() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    try {
                        sorter.setRowFilter(RowFilter.regexFilter(document.getText(0, document.getLength()).toString()));
                    } catch (BadLocationException el) {
                        el.printStackTrace();
                    }
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (document.getLength() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    try {
                        sorter.setRowFilter(RowFilter.regexFilter(document.getText(0, document.getLength()).toString()));
                    } catch (BadLocationException el) {
                        el.printStackTrace();
                    }
                }
            }
        });
        data.removeColumn(data.getColumnModel().getColumn(1));
        data.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if(e.getClickCount() == 2 && !e.isConsumed()) {
                    e.consume();
                    int row = data.rowAtPoint(e.getPoint());
                    LOG.debug("method: mousePressed row = " + row);
                    if(row >= 0) {
                        Lists order = new Lists();
                        ProductTableModel productModel = (ProductTableModel) data.getModel();
                        Product product = productModel.getProduct(row);
                        order.setItemId(product.getId());
                        LOG.debug("method: mousePressed productId = " + product.getId());
                        String s = JOptionPane.showInputDialog(SelectItemDialog.this, "Введите количество товаров");
                        if(s != null && s.length() >= 0) {
                            try {
                                int count = Integer.parseInt(s);
                                order.setAmount(count);
                                LOG.debug("method: mousePressed amount = " + count);
                                ((InputBillDialog) parent).addLists(order);
                            }catch (NumberFormatException ex) {
                                JOptionPane.showMessageDialog(SelectItemDialog.this, "Введите число", "Error", JOptionPane.ERROR_MESSAGE);
                                LOG.error(ex.getMessage());
                            }
                        }
                    }

                }
            }
        });
        JScrollPane scrollPane = new JScrollPane(data);
        panel.add(scrollPane, BorderLayout.CENTER);
        panel.setBorder(BorderFactory.createTitledBorder("Товары"));
       /* JButton close = new JButton("Завершить");
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SelectItemDialog.this.dispose();
            }
        });
        panel.add(close, BorderLayout.SOUTH);
        panel.add(new FilterPanel(), BorderLayout.NORTH);*/
        return panel;
    }

    private JPanel createActionPanel() {
        JPanel panel = new JPanel();
        JButton addItems = new JButton("Создать товар");
        addItems.setPreferredSize(new Dimension(Dimensions.TOOLBAR_BUTTON_WIDTH,
                Dimensions.TOOLBAR_BUTTON_HEIGHT));
        ImageUtils.addButtonIcon(addItems,
                new Dimension(Dimensions.TOOLBAR_ICON_WIDTH,
                        Dimensions.TOOLBAR_ICON_HEIGHT), "./assets/icons/add.png");
        addItems.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AddItemDialog(SelectItemDialog.this, DatabaseSchema.TABLE_PRODUCTS, null, false).setVisible(true);
                /*data.setModel(new ResultSetTableModel(DAOFactory.getMySQLDAOFactory().getProductDAO().selectProductResultSet(),
                        DatabaseUtils.getColumnNamesForTable(DatabaseSchema.TABLE_PRODUCTS)));
                data.removeColumn(data.getColumnModel().getColumn(1));
                data.repaint();*/
                ((ProductTableModel) data.getModel()).updateModel();
            }
        });
        JButton close = new JButton("Создать категорию");
        ImageUtils.addButtonIcon(close,
                new Dimension(Dimensions.TOOLBAR_ICON_WIDTH,
                        Dimensions.TOOLBAR_ICON_HEIGHT), "./assets/icons/folder.png");
        /* Close - button for category creation ! */
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AddItemDialog(SelectItemDialog.this, DatabaseSchema.TABLE_CATEGORIES, null, false).setVisible(true);
            }
        });
        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(addItems).cell(close).fillX();
        gbl.constraints(addItems).weightx = 0.5;
        gbl.constraints(addItems).fill = GridBagConstraints.HORIZONTAL;
        gbl.constraints(close).weightx = 0.5;
        gbl.doneAndPushEverythingToTop();
        //panel.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        return panel;
    }

    private JPanel createMainPanel() {
        JButton close = new JButton("Готово");
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SelectItemDialog.this.dispose();
            }
        });

        close.setPreferredSize(new Dimension(Dimensions.TOOLBAR_BUTTON_WIDTH,
                Dimensions.TOOLBAR_BUTTON_HEIGHT));
        ImageUtils.addButtonIcon(close,
                new Dimension(Dimensions.TOOLBAR_ICON_WIDTH,
                        Dimensions.TOOLBAR_ICON_HEIGHT), "./assets/icons/left.png");
        close.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));

        JPanel panel = new JPanel();
        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        filterPanel = new FilterPanel();
        gbl.row().cellXRemainder(filterPanel).fillX();
        gbl.row().cellXRemainder(createActionPanel()).fillX();
        JPanel tablePanel = createTablePanel();
        gbl.row().cellXRemainder(tablePanel).fillX();
        gbl.row().cellXRemainder(close).fillX();

        gbl.constraints(tablePanel).weightx = 1;
        gbl.constraints(tablePanel).weighty = 1;
        gbl.constraints(tablePanel).fill = GridBagConstraints.BOTH;
        gbl.constraints(close).anchor = GridBagConstraints.PAGE_END;
        gbl.done();
        return panel;
    }

    public void loadData() {
        ResultSet resultSet = ((ResultSetTableModel) data.getModel()).getResultSet();
        if(resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
            }
        }
        ((ResultSetTableModel) data.getModel()).setResultSet(DAOFactory.getMySQLDAOFactory().getProductDAO().selectProductResultSet());
        data.repaint();
    }
    private Window parent;
    private JTable data;
    private FilterPanel filterPanel;
    private Document document;
}
