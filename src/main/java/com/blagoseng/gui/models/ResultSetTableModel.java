package com.blagoseng.gui.models;

import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import javax.swing.table.AbstractTableModel;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 16:16
 */
public class ResultSetTableModel extends AbstractTableModel {
    private static Logger log = Logger.getLogger(ResultSetTableModel.class.getName());

    public ResultSetTableModel(ResultSet resultSet, String[] columnNames) {
        this.resultSet = resultSet;
        this.columnNames = columnNames;
        try {
            metaData = this.resultSet.getMetaData();
            if(metaData == null) throw new NullPointerException("ResultSet can't be null!");
            DatabaseUtils.printForeignKeys(metaData.getTableName(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getColumnName(int c) {
        log.debug("method: getColumnName columns: ");
        int i = 0;
        for(String name : columnNames) {
            log.debug("columnName[" + (i++) + "]: " + name);
        }
        log.debug("method: getColumnName index: " + c);
        return columnNames[c];
        /*try {
            return metaData.getColumnName(c + 1);
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }*/
    }

    @Override
    public int getRowCount() {
        try {
            resultSet.last();
            return resultSet.getRow();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getColumnCount() {
        try {
            return metaData.getColumnCount();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            resultSet.absolute(rowIndex + 1);
            return resultSet.getObject(columnIndex + 1);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
        try {
            this.resultSet.beforeFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        fireTableDataChanged();
    }

    private ResultSet resultSet;
    private ResultSetMetaData metaData;
    private String[] columnNames;
}
