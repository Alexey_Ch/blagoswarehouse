package com.blagoseng.dao;

import com.blagoseng.dao.interfaces.InputBillDAO;
import com.blagoseng.dao.model.InputBill;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:08
 */
public class MySQLInputBillDAO implements InputBillDAO {

    @Override
    public int createInputBill(InputBill inputBill) {
        String query =
                "INSERT INTO "
                        + DatabaseSchema.TABLE_INPUT_BILL
                        + " ("
                        + DatabaseSchema.InputBill.F_DATE + ", "
                        + DatabaseSchema.InputBill.F_DESTINATION + ") "
                        + "VALUES( "
                        + inputBill.getDate().getTime() + ", "
                        + inputBill.getObjectId() + " );";

        return DatabaseUtils.executeUpdate(query);
    }

    @Override
    public boolean updateInputBill(InputBill bill) {
        //TODO: Implement this
        return false;
    }

    @Override
    public boolean deleteInputBill(int inputBillId) {
        //TODO: Implement this
        return false;
    }

    @Override
    public InputBill getInputBill(int inputBillId) {
        //TODO: Implement this
        return null;
    }

    @Override
    public List<InputBill> getInputBills() {
        //TODO: Implement this
        return null;
    }
}
