package com.blagoseng.utils;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.MySQLDAOFactory;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 2:28
 */
public final class DatabaseUtils {
    private static Logger log = Logger.getLogger(DatabaseUtils.class.getName());

    public static String[] getColumnNamesForTable(String tableName) {
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_UNITS)) return DatabaseSchema.UNITS_COLUMN_NAMES;
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_EMPLOYERS)) return DatabaseSchema.EMPLOYERS_COLUMN_NAMES;
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_PRODUCTS)) return DatabaseSchema.PRODUCTS.PRODUCTS_COLUMN_NAMES;
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_OBJECTS)) return DatabaseSchema.OBJECTS_COLUMN_NAMES;
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_POSTS)) return DatabaseSchema.POSTS_COLUMN_NAMES;
        if(tableName.equalsIgnoreCase(DatabaseSchema.TABLE_CATEGORIES)) return DatabaseSchema.CATEGORIES.CATEGORIES_COLUMN_NAMES;

        throw new IllegalArgumentException("Not table with name: " + tableName);
    }

    public static String getTableTitle(String tableName) {
        String title;
        switch (tableName) {
            case DatabaseSchema.TABLE_UNITS:
                title = DatabaseSchema.UNITS.TABLE_TITLE;
                break;
            case DatabaseSchema.TABLE_EMPLOYERS:
                title = DatabaseSchema.EMPLOYERS.TABLE_TITLE;
                break;
            case DatabaseSchema.TABLE_PRODUCTS:
                title = DatabaseSchema.PRODUCTS.TABLE_TITLE;
                break;
            case DatabaseSchema.TABLE_OBJECTS:
                title = DatabaseSchema.OBJECTS.TABLE_TITLE;
                break;
            case DatabaseSchema.TABLE_POSTS:
                title = DatabaseSchema.POSTS.TABLE_TITLE;
                break;
            case DatabaseSchema.TABLE_CATEGORIES:
                title = DatabaseSchema.CATEGORIES.TABLE_TITLE;
                break;
            default:
                throw new IllegalArgumentException("Not table with name: " + tableName);
        }
        return title;
    }

    public static void printForeignKeys(String tableName) throws SQLException {
        Connection connection = ((MySQLDAOFactory) DAOFactory.getDAOFactory(DAOFactory.MYSQL)).getConnection();
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet foreignKeys = metaData.getImportedKeys(connection.getCatalog(), null, tableName);
        while (foreignKeys.next()) {
            String fkTableName = foreignKeys.getString("FKTABLE_NAME");
            String fkColumnName = foreignKeys.getString("FKCOLUMN_NAME");
            String pkTableName = foreignKeys.getString("PKTABLE_NAME");
            String pkColumnName = foreignKeys.getString("PKCOLUMN_NAME");
            System.out.println(fkTableName + "." + fkColumnName + " -> " + pkTableName + "." + pkColumnName);
        }
    }

    public static String createSelectQuery(List<String> fieldList,
                                           List<String> tableList,
                                           String optionalTale) {
        StringBuilder query = new StringBuilder("SELECT ");
        query.append(createStringFromListWithDelimiter(fieldList, ", "));
        query.append(" FROM ");
        query.append(createStringFromListWithDelimiter(tableList, ", "));
        if(optionalTale != null) {
            query.append(optionalTale);
        }
        query.append(';');
        return query.toString();
    }

    private static String createStringFromListWithDelimiter(List<String> parts, String delimiter) {
        if(parts == null) return "";
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for(String s : parts) {
            builder.append(s);
            if(i < parts.size() -  1) {
                builder.append(delimiter);
            }
            i++;
        }
        return builder.toString();
    }

    public static ResultSet selectAllFromTable(String tableName) {
        String query = Strings.SQL.SELECT
                       + Strings.SQL.ALL
                       + Strings.SQL.FROM
                       + tableName
                       + Strings.SQL.SEMICOLON;
        Connection conn = DAOFactory.getMySQLDAOFactory().getConnection();
        Statement statement;
        ResultSet resultSet = null;
        try {
            statement = conn.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public static boolean executeUpdateQuery(String query) {
        return (executeUpdate(query) > 0);
    }

    public static int executeUpdate(String query) {
        ResultSet generatedKeys = null;
        log.debug("method: executeUpdateQuery query: " + query);
        Connection c = DAOFactory.getMySQLDAOFactory().getConnection();
        int res = -1;
        Statement stat = null;
        try {
            stat = c.createStatement();
            res = stat.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            if(res > 0) {
                generatedKeys = stat.getGeneratedKeys();
                if (generatedKeys.next()) {
                    res = (int) generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Creating user failed, no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        log.debug("method: executeUpdateQuery result: " + res);
        return res;
    }

    public static ResultSet executeQuery(String query) {
        Connection conn;
        conn = ((MySQLDAOFactory) DAOFactory.getDAOFactory(DAOFactory.MYSQL)).getConnection();
        log.debug("method: executeQuery query: " + query);
        ResultSet resultSet = null;
        try {
            Statement statement = conn.createStatement();
            resultSet = statement.executeQuery(query);
           /// statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return resultSet;
    }
}
