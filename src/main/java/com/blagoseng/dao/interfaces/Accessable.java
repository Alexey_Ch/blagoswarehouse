package com.blagoseng.dao.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 10.06.13
 * Time: 1:25
 */
public interface Accessable {
    public Object getPropertyByIndex(int index);
}
