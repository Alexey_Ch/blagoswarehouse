package com.blagoseng.dao;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.interfaces.UnitDAO;
import com.blagoseng.dao.model.Post;
import com.blagoseng.dao.model.Unit;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:19
 */
public class MySQLUnitDAO implements UnitDAO {
    private static Logger log = Logger.getLogger(MySQLUnitDAO.class.getName());

    @Override
    public boolean insertUnit(Unit unit) {
        String query = Strings.SQL.INSERT
                + DatabaseSchema.TABLE_UNITS
                + Strings.SQL.START_BRACKET
                + DatabaseSchema.UNITS.F_NAME
                + Strings.SQL.COMMA
                + DatabaseSchema.UNITS.F_SHORT_NAME
                + Strings.SQL.END_BRACKET
                + Strings.SQL.VALUES
                + Strings.SQL.BACK_QUOTE
                + unit.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA
                + Strings.SQL.BACK_QUOTE
                + unit.getShortName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.END_BRACKET
                + Strings.SQL.SEMICOLON;
        log.debug("method: insertUnit Unit: " + unit.toString());
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean deleteUnit(Unit unit) {
        String query = Strings.SQL.DELETE
                + DatabaseSchema.TABLE_UNITS
                + Strings.SQL.WHERE
                + DatabaseSchema.UNITS.F_ID
                + Strings.SQL.IS
                + unit.getId();
        log.debug("method: deleteUnit - unitId = " + unit.getId());
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean updateUnit(Unit unit) {
        String query = Strings.SQL.UPDATE
                + DatabaseSchema.TABLE_UNITS
                + Strings.SQL.SET
                + DatabaseSchema.UNITS.F_NAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + unit.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA
                + DatabaseSchema.UNITS.F_SHORT_NAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + unit.getShortName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.WHERE
                + DatabaseSchema.UNITS.F_ID
                + Strings.SQL.IS
                + unit.getId()
                + Strings.SQL.SEMICOLON;
        log.debug("method: updateUnit unit: " + unit);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public Unit[] getUnits() {
        ResultSet resultSet = selectUnitResultSet();
        List<Unit> unitList = new ArrayList<Unit>();
        log.debug("method: getUnits");
        Unit post;

        try {
            resultSet.beforeFirst();
            resultSet.next();
            while (!resultSet.isAfterLast()) {
                post = new Unit();
                post.setId(resultSet.getInt(DatabaseSchema.UNITS.F_ID));
                post.setName(resultSet.getString(DatabaseSchema.UNITS.F_NAME));
                post.setShortName(resultSet.getString(DatabaseSchema.UNITS.F_SHORT_NAME));
                unitList.add(post);
                resultSet.next();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }

        return unitList.toArray(new Unit[unitList.size()]);
    }

    @Override
    public Unit getUnit(int unitId) {
        String query = Strings.SQL.SELECT
                + Strings.SQL.ALL
                + Strings.SQL.FROM
                + DatabaseSchema.TABLE_UNITS
                + Strings.SQL.WHERE
                + DatabaseSchema.UNITS.F_ID
                + Strings.SQL.IS
                + unitId;
        log.debug("method: getUnit unitId: " + unitId);
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        Unit unit = null;
        try {
            resultSet.beforeFirst();
            if(resultSet.next()) {
                unit = new Unit();
                unit.setId(unitId);
                unit.setName(resultSet.getString(DatabaseSchema.UNITS.F_NAME));
                unit.setShortName(resultSet.getString(DatabaseSchema.UNITS.F_SHORT_NAME));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return unit;
    }

    @Override
    public ResultSet selectUnitResultSet() {
        String query = Strings.SQL.SELECT +
                Strings.SQL.ALL +
                Strings.SQL.FROM +
                DatabaseSchema.TABLE_UNITS +
                Strings.SQL.SEMICOLON;
        conn = ((MySQLDAOFactory) DAOFactory.getDAOFactory(DAOFactory.MYSQL)).getConnection();

        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = conn.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return resultSet;
    }


    private Connection conn;
}
