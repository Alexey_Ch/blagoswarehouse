package com.blagoseng.gui.models;

import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.MySQLProductDAO;
import com.blagoseng.dao.model.Category;
import com.blagoseng.dao.model.Product;
import com.blagoseng.dao.model.Unit;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import com.blagoseng.utils.ListUtils;
import org.apache.log4j.Logger;

import javax.swing.table.AbstractTableModel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 10.07.13
 * Time: 4:06
 */
public class ProductsRemainderTableModel extends AbstractTableModel {
    private static final Logger LOG  = Logger.getLogger(ProductsRemainderTableModel.class);

    public ProductsRemainderTableModel() {
        updateData();
    }

    @Override
    public int getRowCount() {
        return products.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        //TODO: Implement this
        Object value;
        switch (columnIndex) {
            case 0: {value = products.get(rowIndex).getName(); break;}
            case 1: {value = products.get(rowIndex).getLabel(); break;}
            case 2: {value = getCategoryById(products.get(rowIndex).getCategoryId()).getName(); break;}
            case 3: {value = ListUtils.getUnitById(Arrays.asList(units), products.get(rowIndex).getUnitId()).getName(); break;}
            case 4: {value = results.get(products.get(rowIndex).getId()); break;}
            default: value = "";
        }
        return value;
    }

    @Override
    public String getColumnName(int column) {
        String columnName;
        switch (column) {
            case 0: {columnName = "Товар"; break;}
            case 1: {columnName = "Метка"; break;}
            case 2: {columnName = "Категория"; break;}
            case 3: {columnName = "Ед. изм."; break;}
            case 4: {columnName = "Количество"; break;}
            default: columnName = "";
        }
        return columnName;
    }

    public void updateData() {
        String query = "select item_id, sum(count) as total from Lists  group by item_id;";
         results = new HashMap<>();
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        try {
            resultSet.beforeFirst();
            products = new ArrayList<>();
            MySQLProductDAO dao = (MySQLProductDAO) DAOFactory.getMySQLDAOFactory().getProductDAO();
            while (resultSet.next()) {
                int id = resultSet.getInt(DatabaseSchema.Lists.F_ITEM_ID);
                results.put(id, resultSet.getInt("total"));
                products.add(dao.getProduct(id));
            }
            resultSet.close();
        } catch (SQLException e) {

        }
        categories = DAOFactory.getMySQLDAOFactory().getCategoryDAO().getAllCategories();
        units = DAOFactory.getMySQLDAOFactory().getUnitDAO().getUnits();
        fireTableDataChanged();
    }

    public int getProductId(int row) {
        return products.get(row).getId();
    }

    private Category getCategoryById(int id) {
        for(Category p : categories) {
            if(p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    /*private Unit getUnitById(int id) {
        for(Unit p : units) {
            if(p.getId() == id) {
                return p;
            }
        }
        return null;
    }*/

    private List<Product> products;
    private Unit[] units;

    private Map<Integer, Integer> results;
    private List<Category> categories;
}
