package com.blagoseng.dao.model;

import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.interfaces.Accessable;

import java.io.Serializable;
import java.lang.*;
import java.lang.Object;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:24
 */
public class Product implements Serializable, Accessable {
    private int itemId;
    private String name;
    private String label;
    private int categoryId;
    private int unitId;

    public int getId() {
        return itemId;
    }

    public void setId(int itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    @Override
    public Object getPropertyByIndex(int index) {
        switch (index) {
            case 0: return itemId;
            case 1: return name;
            case 2: return label;
            case 3: return DAOFactory.getMySQLDAOFactory()
                    .getCategoryDAO().findCategory(categoryId).getName();
            case 4: return unitId;
            default:return null;
        }
    }

    @Override
    public String toString() {
        return "Product{" +
                "itemId=" + itemId +
                ", name='" + name + '\'' +
                ", label='" + label + '\'' +
                ", categoryId=" + categoryId +
                ", unitId=" + unitId +
                '}';
    }
}
