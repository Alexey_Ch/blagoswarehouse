package com.blagoseng.gui.dialogs;

import com.blagoseng.dao.model.Lists;
import com.blagoseng.gui.models.RemainderDescriptionTableModel;
import com.blagoseng.gui.models.ResultSetTableModel;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 11.07.13
 * Time: 20:53
 */
public class RemainderDescription extends JDialog {
    private static final Logger LOG = Logger.getLogger(RemainderDescription.class);

    public RemainderDescription(int itemId, Component parent) {
        super((Dialog) null, "Формирование остатка", true);
        this.itemId = itemId;
        setContentPane(createMainPanel());
        pack();
        setLocationRelativeTo(parent);
    }

    private JPanel createMainPanel() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        data = new JTable(new RemainderDescriptionTableModel(itemId));
        JScrollPane scroll = new JScrollPane(data);
        mainPanel.add(scroll, BorderLayout.CENTER);
        return mainPanel;
    }

    private JTable data;
    private int itemId;
}
