package com.blagoseng.dao;

import com.blagoseng.dao.interfaces.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 13:13
 */
public class MySQLDAOFactory extends DAOFactory {
    private static Connection connection = null;

    public MySQLDAOFactory() {
        connection = createConnection();
    }

    public Connection getConnection() {
        if(connection == null) {
            connection = createConnection();
        } else {
            try {
                if(connection.isClosed() || !connection.isValid(1000)) {
                    connection = createConnection();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    private Connection createConnection() {
        Connection connection = null;
        Properties properties = new Properties();
        String driver = null;
        String databaseURL = null;
        String userName = null;
        String password = null;
        try {
            properties.load(getClass().getResourceAsStream("/database.properties"));
            driver = properties.getProperty("jdbc.drivers");
            databaseURL = properties.getProperty("jdbc.url");
            userName = properties.getProperty("jdbc.username");
            password = properties.getProperty("jdbc.password");

            if(driver != null) {
                System.setProperty("jdbc.drivers", driver);
            } else {
                throw new NullPointerException("Couldn't find JDBC driver");
            }
            connection = DriverManager.getConnection(databaseURL, userName, password);
        } catch (SQLException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }
        if(connection == null) throw new NullPointerException("Couldn't get connection");
        return connection;
    }

    @Override
    public CategoryDAO getCategoryDAO() {
        return new MySQLCategoryDAO();
    }

    @Override
    public EmployerDAO getEmployerDAO() {
        return new MySQLEmployerDAO();
    }

    @Override
    public InputBillDAO getInputBillDAO() {
        return new MySQLInputBillDAO();
    }

    @Override
    public RemainderDescriptionDAO getRemainderDescriptionDAO() {
        return new MySQLRemainderDescriptionDAO();
    }

    @Override
    public OutBillDAO getOutBillDAO() {
        return new MySQLOutBillDAO();
    }

    @Override
    public ObjectDAO getObjectDAO() {
        return new MySQLObjectDAO();
    }

    @Override
    public PostDAO getPostDAO() {
        return new MySQLPostDAO();
    }

    @Override
    public ProductDAO getProductDAO() {
        return new MySQLProductDAO();
    }

    @Override
    public UnitDAO getUnitDAO() {
        return new MySQLUnitDAO();
    }

    @Override
    public RightDAO getRightDAO() {
        return new MySQLRightDAO();
    }

    @Override
    public ListDAO getListDAO() {
        return new MySQLListDAO();
    }
}
