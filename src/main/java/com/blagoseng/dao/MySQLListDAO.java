package com.blagoseng.dao;

import com.blagoseng.dao.interfaces.ListDAO;
import com.blagoseng.dao.model.InputBill;
import com.blagoseng.dao.model.Lists;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 10.07.13
 * Time: 2:27
 */
public class MySQLListDAO implements ListDAO {
    @Override
    public int createList(Lists lists) {
        String query =
                    "INSERT INTO "
                + DatabaseSchema.TABLE_LISTS
                + " ("
                + DatabaseSchema.Lists.F_ITEM_ID + ", "
                + DatabaseSchema.Lists.F_BILL_ID + ", "
                + DatabaseSchema.Lists.F_AMOUNT + ") "
                + "VALUES( "
                + lists.getItemId() + ", "
                + lists.getBillId() + ", "
                + lists.getAmount() + " );";

        return DatabaseUtils.executeUpdate(query);
    }

    @Override
    public boolean updateList(Lists lists) {
        //TODO: Implement this
        return false;
    }

    @Override
    public boolean deleteList(int listId) {
        //TODO: Implement this
        return false;
    }

    @Override
    public InputBill getLists(int listId) {
        //TODO: Implement this
        return null;
    }

    @Override
    public List<Lists> getLists() {
        //TODO: Implement this
        return null;
    }
}
