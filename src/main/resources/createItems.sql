CREATE TABLE `Items` (
  `item_id` INT NOT NULL AUTO_INCREMENT,
  `name` TEXT,
  `label` TEXT,
  `ctegory_id` INT,
  `unit_id` INT,
  PRIMARY KEY  (`item_id`)
);

ALTER TABLE `Items` ADD CONSTRAINT `Items_fk1` FOREIGN KEY (`ctegory_id`) REFERENCES Categories(`category_id`);
ALTER TABLE `Items` ADD CONSTRAINT `Items_fk2` FOREIGN KEY (`unit_id`) REFERENCES Units(`unit_id`);

ALTER TABLE `Lists` ADD CONSTRAINT `Lists_fk2` FOREIGN KEY (`item_id`) REFERENCES Items(`item_id`);