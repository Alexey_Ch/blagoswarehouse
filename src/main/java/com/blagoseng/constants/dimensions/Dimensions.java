package com.blagoseng.constants.dimensions;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 07.06.13
 * Time: 22:53
 */
public final class Dimensions {
    public static final int MENU_ITEM_HEIGHT = 40;
    public static final int MENU_ITEM_WIDTH = 120;

    public static final int  MENU_ITEM_ICON_HEIGHT = 25;
    public static final int  MENU_ITEM_ICON_WIDTH = 25;

    public static final int TOOLBAR_BUTTON_HEIGHT = 40;
    public static final int TOOLBAR_BUTTON_WIDTH = 120;

    public static final int TOOLBAR_ICON_HEIGHT = 30;
    public static final int TOOLBAR_ICON_WIDTH = 30;
}
