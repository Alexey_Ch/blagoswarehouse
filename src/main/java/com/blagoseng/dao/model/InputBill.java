package com.blagoseng.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 09.07.13
 * Time: 9:15
 */
public class InputBill implements Serializable {
    private int billId;
    private Date date;
    private int objectId;
    private List<Lists> orderList;

    public InputBill() {
        date = new Date();
        orderList = new ArrayList<>();
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
        for(Lists l : orderList) {
            l.setBillId(this.billId);
        }
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public List<Lists> getListsList() {
        return orderList;
    }

    public void setListsList(List<Lists> orderList) {
        this.orderList = orderList;
    }

    @Override
    public String toString() {
        return "InputBill{" +
                "billId=" + billId +
                ", date=" + date +
                ", objectId=" + objectId +
                ", orderList=" + orderList +
                '}';
    }
}
