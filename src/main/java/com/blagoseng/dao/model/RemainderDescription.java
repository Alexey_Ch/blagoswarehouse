package com.blagoseng.dao.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 11.07.13
 * Time: 22:27
 */
public class RemainderDescription implements Serializable {
    private int destination;
    private int itemId;
    private int amount;

    public int getDestination() {
        return destination;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "RemainderDescription{" +
                "destination=" + destination +
                ", itemId=" + itemId +
                ", amount=" + amount +
                '}';
    }
}

