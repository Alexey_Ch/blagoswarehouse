package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.Post;

import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 13:01
 */
public interface PostDAO {
    public ResultSet selectPostResultSet();
    public boolean insertPost(String postName);
    public boolean deletePost(int postId);
    public boolean updatePost(Post post);
    public Post[] getPosts();
    public Post getPost(int postId);
}
