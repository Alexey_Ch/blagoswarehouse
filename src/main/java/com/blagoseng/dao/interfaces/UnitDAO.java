package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.Unit;

import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 13:03
 */
public interface UnitDAO {
    public boolean insertUnit(Unit unit);
    public boolean deleteUnit(Unit unit);
    public boolean updateUnit(Unit unit);
    public Unit getUnit(int unitId);
    public Unit[] getUnits();
    public ResultSet selectUnitResultSet();
}
