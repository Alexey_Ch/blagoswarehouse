package com.blagoseng.gui.dialogs;

import com.blagoseng.dao.interfaces.ListDAO;
import com.blagoseng.dao.model.Lists;
import com.blagoseng.dao.model.Object;
import com.blagoseng.constants.dimensions.Dimensions;
import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.model.InputBill;
import com.blagoseng.gui.Main;
import com.blagoseng.gui.models.InputBillTableModel;
import com.blagoseng.interfaces.HasName;
import com.blagoseng.utils.ImageUtils;
import org.apache.log4j.Logger;
import org.painlessgridbag.PainlessGridBag;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 04.07.13
 * Time: 4:00
 */
public class InputBillDialog extends JDialog {
    private static final Logger LOG = Logger.getLogger(InputBillDialog.class);
    public InputBillDialog(JFrame parent) {
        super(parent, "Приход товара", true);
        this.parent = parent;

        bill = new InputBill();

        add(/*createPanel()*/ /*createTablePanel()*/createMainPanel(), BorderLayout.CENTER);
        setPreferredSize(new Dimension(700, 600));
        pack();
        setLocationRelativeTo(null);
    }

    private JPanel createCaption() {
        JPanel caption = new JPanel();
        JLabel title = new JLabel("Приходная накладная");
        title.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
        caption.add(title);
        caption.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        return caption;
    }

    private JPanel createBillDetails() {
        JPanel panel = new JPanel();
        JLabel number = new JLabel("Номер: ");
        JLabel date = new JLabel("Дата: ");
        JLabel destination = new JLabel("Назначение");
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        objectsCombo = new JComboBox<HasName>(DAOFactory.getMySQLDAOFactory()
                .getObjectDAO().getObjects());
        objectsCombo.setRenderer(new AddItemDialog.MyCombBoxRenderer());
        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(number).cell(new JLabel("3")).fillX();
        gbl.row().cell(date).cell(new JLabel(sdf.format(today))).fillX();
        gbl.row().cell(destination).cell(objectsCombo).fillX();
        gbl.doneAndPushEverythingToTop();
        panel.setBorder(BorderFactory.createTitledBorder("Реквизиты документа"));
        return panel;
    }

    private JPanel createActionPanel() {
        JPanel panel = new JPanel();
        JButton addItems = new JButton("Добавить товар");
        addItems.setPreferredSize(new Dimension(Dimensions.TOOLBAR_BUTTON_WIDTH,
                                                Dimensions.TOOLBAR_BUTTON_HEIGHT));
        ImageUtils.addButtonIcon(addItems,
                new Dimension(Dimensions.TOOLBAR_ICON_WIDTH,
                              Dimensions.TOOLBAR_ICON_HEIGHT), "./assets/icons/add-to-cart.png");
        addItems.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SelectItemDialog(InputBillDialog.this).setVisible(true);

            }
        });
        JButton close = new JButton("Закрыть");
        ImageUtils.addButtonIcon(close,
                new Dimension(Dimensions.TOOLBAR_ICON_WIDTH,
                        Dimensions.TOOLBAR_ICON_HEIGHT), "./assets/icons/multiply.png");
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InputBillDialog.this.dispose();
            }
        });
        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cell(addItems).cell(close).fillX();
        gbl.constraints(addItems).weightx = 0.5;
        gbl.constraints(addItems).fill = GridBagConstraints.HORIZONTAL;
        gbl.constraints(close).weightx = 0.5;
        gbl.doneAndPushEverythingToTop();
        return panel;
    }

    private JPanel createMainPanel() {
        JPanel panel = new JPanel();
        doIt = new JButton("Провести");
        doIt.setEnabled(false);
        doIt.setPreferredSize(new Dimension(Dimensions.TOOLBAR_BUTTON_WIDTH,
                                            Dimensions.TOOLBAR_BUTTON_HEIGHT));
        ImageUtils.addButtonIcon(doIt,
                new Dimension(Dimensions.TOOLBAR_ICON_WIDTH,
                        Dimensions.TOOLBAR_ICON_HEIGHT), "./assets/icons/diskette.png");
        doIt.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));
        doIt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //JOptionPane.showMessageDialog(InputBillDialog.this, "Провести!");
                bill.setObjectId(((Object) objectsCombo.getSelectedItem()).getId());
                int billId = DAOFactory.getMySQLDAOFactory()
                        .getInputBillDAO().createInputBill(bill);
                LOG.debug("Провести: " + billId);
                bill.setBillId(billId);
                ListDAO listDAO = DAOFactory.getMySQLDAOFactory().getListDAO();
                for(Lists l : bill.getListsList()) {
                    listDAO.createList(l);
                }
                ((Main) parent).updateTableReminder();
                InputBillDialog.this.dispose();
            }
        });
        PainlessGridBag gbl = new PainlessGridBag(panel, false);
        gbl.row().cellXRemainder(createCaption()).fillX();
        gbl.row().cellXRemainder(createBillDetails()).fillX();
        gbl.row().cellXRemainder(createActionPanel()).fillX();
        JPanel tablePanel = createTablePanel();
        gbl.row().cellXRemainder(tablePanel).fillX();
        gbl.row().cellXRemainder(doIt).fillX();
        gbl.constraints(tablePanel).weightx = 1;
        gbl.constraints(tablePanel).weighty = 1;
        gbl.constraints(tablePanel).fill = GridBagConstraints.BOTH;
        gbl.constraints(doIt).anchor = GridBagConstraints.PAGE_END;

        gbl.done();
        return panel;
    }

    private JPanel createTablePanel() {
        JPanel panel = new JPanel(new BorderLayout());
        data = new JTable();
        data.setModel(new InputBillTableModel(bill));
        JScrollPane scrollPane = new JScrollPane(data);
        panel.add(scrollPane);
        panel.setBorder(BorderFactory.createTitledBorder("Товары"));
        return panel;
    }

    public void addLists(Lists order) {
        doIt.setEnabled(true);
        ((InputBillTableModel) data.getModel()).addLists(order);
    }
    private JFrame parent;
    private JPanel panel;
    private JComboBox objectsCombo;
    private InputBill bill;
    private JTable data;
    private JButton doIt;
}
