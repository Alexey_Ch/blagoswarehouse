package com.blagoseng.dao.model;

import com.blagoseng.interfaces.HasName;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:23
 */
public class Object implements Serializable, HasName {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Object{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
