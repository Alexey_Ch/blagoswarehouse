package com.blagoseng.dao.model;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 10.07.13
 * Time: 2:23
 */
public class Lists {
    private int id;
    private int billId;
    private int itemId;
    private int amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Lists{" +
                "id=" + id +
                ", billId=" + billId +
                ", itemId=" + itemId +
                ", amount=" + amount +
                '}';
    }
}
