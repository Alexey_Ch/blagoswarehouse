package com.blagoseng.gui;

import com.blagoseng.constants.dimensions.Dimensions;
import com.blagoseng.constants.strings.Strings;
import com.blagoseng.gui.dialogs.CatalogDialog;
import com.blagoseng.gui.dialogs.InputBillDialog;
import com.blagoseng.gui.models.ProductsRemainderTableModel;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.ImageUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 07.06.13
 * Time: 20:35
 */
public class Main extends JFrame {
    public static void main(String[] args) {
        changeLF();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Main frame = new Main();
                frame.setTitle("Warehouse");
                frame.setJMenuBar(frame.createJMenuBar());
                frame.add(frame.createJToolBar(), BorderLayout.NORTH);
                frame.add(frame.createMainTable(), BorderLayout.CENTER);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
                frame.setVisible(true);
            }
        });


    }

    private JMenuBar createJMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        menuBar.setPreferredSize(new Dimension(menuBar.getWidth(), Dimensions.MENU_ITEM_HEIGHT));

        Dimension menuItemDimension = new Dimension(Dimensions.MENU_ITEM_WIDTH,
                                                    Dimensions.MENU_ITEM_HEIGHT);
        Dimension menuIconSize = new Dimension(Dimensions.MENU_ITEM_ICON_WIDTH,
                                               Dimensions.MENU_ITEM_ICON_HEIGHT);
        Dimension dim = new Dimension(190,
                Dimensions.MENU_ITEM_HEIGHT);

        JMenu operationMenu = new JMenu(Strings.OPERATIONS_MENU);
        JMenuItem incomingItem = new JMenuItem(Strings.INCOMING_MENU_ITEM);
        incomingItem.setPreferredSize(menuItemDimension);
        incomingItem.addActionListener(new InputBillEvent());
        JMenuItem outgoItem = new JMenuItem(Strings.OUTGO_MENU_ITEM);
        outgoItem.setPreferredSize(menuItemDimension);
        JMenuItem returnItem = new JMenuItem(Strings.RETURN_MENU_ITEM);
        returnItem.setPreferredSize(menuItemDimension);
        try {

            BufferedImage bufImage = ImageIO.read(new File("./assets/icons/download.png"));
            incomingItem.setIcon(
                    new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));

            bufImage = ImageIO.read(new File("./assets/icons/upload.png"));
            outgoItem.setIcon(
                     new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));
            bufImage = ImageIO.read(new File("./assets/icons/recycle.png"));
            returnItem.setIcon(
                    new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));

        } catch (IOException e) {

        }
        operationMenu.add(incomingItem);
        operationMenu.add(outgoItem);
        operationMenu.add(returnItem);

        JMenu documentsMenu = new JMenu(Strings.DOCUMENTS_MENU);
        JMenuItem inputBillItem = new JMenuItem(Strings.INCOMING_BILLS_MENU_ITEM);

        inputBillItem.setPreferredSize(dim);
        JMenuItem outgoBillItem = new JMenuItem(Strings.OUTGO_BILLS_MENU_ITEM);
        outgoBillItem.setPreferredSize(dim);
        JMenuItem returnBillItem = new JMenuItem(Strings.RETURN_BILLS_MENU_ITEM);
        returnBillItem.setPreferredSize(dim);
        try {
            BufferedImage bufImage = ImageIO.read(new File("./assets/icons/documents.png"));
            ImageIcon docsIcon = new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width,
                                                                     menuIconSize.height));
            inputBillItem.setIcon(docsIcon);
            outgoBillItem.setIcon(docsIcon);
            returnBillItem.setIcon(docsIcon);

        } catch (IOException e) {

        }
        documentsMenu.add(inputBillItem);
        documentsMenu.add(outgoBillItem);
        documentsMenu.add(returnBillItem);


        JMenu catalogsMenu = new JMenu(Strings.CATALOGS_MENU);

        JMenuItem staffCatalogItem = new JMenuItem(Strings.STAFF_CATALOG_MENU_ITEM);
        staffCatalogItem.setPreferredSize(dim);
        staffCatalogItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CatalogDialog(Main.this, true, DatabaseSchema.TABLE_EMPLOYERS).setVisible(true);
            }
        });

        JMenuItem productCatalogItem = new JMenuItem(Strings.PRODUCT_CATALOG_MENU_ITEM);
        productCatalogItem.setPreferredSize(dim);
        productCatalogItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CatalogDialog(Main.this, true, DatabaseSchema.TABLE_PRODUCTS).setVisible(true);
            }
        });

        JMenuItem unitsCatalogItem = new JMenuItem(Strings.UNITS_CATALOG_MENU_ITEM);
        unitsCatalogItem.setPreferredSize(dim);
        unitsCatalogItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CatalogDialog(Main.this, true, DatabaseSchema.TABLE_UNITS).setVisible(true);
            }
        });

        JMenuItem postsCatalogItem = new JMenuItem(Strings.POSTS_CATALOG_MENU_ITEM);
        postsCatalogItem.setPreferredSize(dim);
        postsCatalogItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CatalogDialog(Main.this, true, DatabaseSchema.TABLE_POSTS).setVisible(true);
            }
        });

        JMenuItem categoriesCatalogItem = new JMenuItem(Strings.CATEGORIES_CATALOG_MENU_ITEM);
        categoriesCatalogItem.setPreferredSize(dim);
        categoriesCatalogItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CatalogDialog(Main.this, true, DatabaseSchema.TABLE_CATEGORIES).setVisible(true);
            }
        });

        JMenuItem objectsCatalogItem = new JMenuItem(Strings.OBJECTS_CATALOG_MENU_ITEM);
        objectsCatalogItem.setPreferredSize(dim);
        objectsCatalogItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CatalogDialog(Main.this, true, DatabaseSchema.TABLE_OBJECTS).setVisible(true);
            }
        });

        try{
            BufferedImage bufImage = ImageIO.read(new File("./assets/icons/users.png"));
            staffCatalogItem.setIcon(
                    new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));

            bufImage = ImageIO.read(new File("./assets/icons/cart.png"));
            productCatalogItem.setIcon(
                    new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));

            bufImage = ImageIO.read(new File("./assets/icons/calculator.png"));
            unitsCatalogItem.setIcon(
                    new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));

            bufImage = ImageIO.read(new File("./assets/icons/briefcase.png"));
            postsCatalogItem.setIcon(
                    new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));

            bufImage = ImageIO.read(new File("./assets/icons/list.png"));
            categoriesCatalogItem.setIcon(
                    new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));

            bufImage = ImageIO.read(new File("./assets/icons/home.png"));
            objectsCatalogItem.setIcon(
                    new ImageIcon(ImageUtils.resize(bufImage, menuIconSize.width, menuIconSize.height)));

        }catch (IOException e) {
            e.printStackTrace();
        }
        catalogsMenu.add(staffCatalogItem);
        catalogsMenu.add(postsCatalogItem);
        catalogsMenu.add(new JSeparator(SwingConstants.HORIZONTAL));
        catalogsMenu.add(productCatalogItem);
        catalogsMenu.add(categoriesCatalogItem);
        catalogsMenu.add(unitsCatalogItem);
        catalogsMenu.add(new JSeparator(SwingConstants.HORIZONTAL));
        catalogsMenu.add(objectsCatalogItem);

        JMenu settingsMenu = new JMenu(Strings.SETTINGS_MENU);

        menuBar.add(operationMenu);
        menuBar.add(documentsMenu);
        menuBar.add(catalogsMenu);
        menuBar.add(settingsMenu);
        return menuBar;
    }

    private JToolBar createJToolBar() {
        JToolBar bar = new JToolBar();
        bar.setOrientation(SwingConstants.HORIZONTAL);
        bar.setFloatable(false);
        JButton incomingBtn = new JButton(Strings.INCOMING_MENU_ITEM);
        Dimension toolbarBtnDimension = new Dimension(Dimensions.TOOLBAR_BUTTON_WIDTH,
                                                      Dimensions.TOOLBAR_BUTTON_HEIGHT);
        incomingBtn.setPreferredSize(toolbarBtnDimension);
        incomingBtn.addActionListener(new InputBillEvent());

        JButton outgoBtn = new JButton(Strings.OUTGO_MENU_ITEM);
        outgoBtn.setPreferredSize(toolbarBtnDimension);

        JButton returnBtn = new JButton(Strings.RETURN_MENU_ITEM);
        returnBtn.setPreferredSize(toolbarBtnDimension);
        try {
            Dimension iconDim = new Dimension(Dimensions.TOOLBAR_ICON_WIDTH,
                    Dimensions.TOOLBAR_ICON_HEIGHT);
            BufferedImage bufImage = ImageIO.read(new File("./assets/incoming.png"));
            incomingBtn.setIcon(new ImageIcon(ImageUtils.resize(bufImage, iconDim.width, iconDim.height)));

            bufImage = ImageIO.read(new File("./assets/outgo.png"));
            outgoBtn.setIcon(new ImageIcon(ImageUtils.resize(bufImage, iconDim.width, iconDim.height)));

            bufImage = ImageIO.read(new File("./assets/return.png"));
            returnBtn.setIcon(new ImageIcon(ImageUtils.resize(bufImage, iconDim.width, iconDim.height)));
        } catch (IOException e) {

        }
        bar.add(incomingBtn);
        bar.add(outgoBtn);
        bar.add(returnBtn);
        bar.add(new JSeparator(SwingConstants.VERTICAL));

        return bar;
    }

    private JPanel createMainTable() {
        tablePanel = new ItemsPanel();
        return tablePanel;
    }

    private static void changeLF() {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }
    }

    private class InputBillEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            new InputBillDialog(Main.this).setVisible(true);

        }
    }

    public void updateTableReminder() {
        ((ProductsRemainderTableModel) tablePanel.getTableData().getModel()).updateData();
    }
    private ItemsPanel tablePanel;
}


