package com.blagoseng.dao;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.interfaces.PostDAO;
import com.blagoseng.dao.model.Post;
import com.blagoseng.dao.model.Right;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:19
 */
public class MySQLPostDAO implements PostDAO {
    private static Logger log = Logger.getLogger(MySQLPostDAO.class.getName());
    public ResultSet selectPostResultSet() {
        String query = Strings.SQL.SELECT +
                       Strings.SQL.ALL +
                       Strings.SQL.FROM +
                       DatabaseSchema.TABLE_POSTS +
                       Strings.SQL.SEMICOLON;

        log.debug("Query: " + query + "\n");
        return DatabaseUtils.executeQuery(query);
    }

    public boolean insertPost(String postName) {
        String query = Strings.SQL.INSERT +
                       DatabaseSchema.TABLE_POSTS +
                       Strings.SQL.START_BRACKET +
                       DatabaseSchema.POSTS.F_NAME +
                       Strings.SQL.END_BRACKET +
                       Strings.SQL.VALUES +
                       Strings.SQL.BACK_QUOTE +
                       postName +
                       Strings.SQL.BACK_QUOTE +
                       Strings.SQL.END_BRACKET +
                       Strings.SQL.SEMICOLON;

        log.debug("method: deletePost -> postName: " + postName);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    public boolean deletePost(int postId) {
        String query = Strings.SQL.DELETE +
                       DatabaseSchema.TABLE_POSTS +
                       Strings.SQL.WHERE +
                       DatabaseSchema.POSTS.F_ID +
                       Strings.SQL.IS + postId +
                       Strings.SQL.SEMICOLON;

        log.debug("method: deletePost -> postId: " + postId);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean updatePost(Post post) {
        String query = Strings.SQL.UPDATE
                + DatabaseSchema.TABLE_POSTS
                + Strings.SQL.SET
                + DatabaseSchema.POSTS.F_NAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + post.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.WHERE
                + DatabaseSchema.POSTS.F_ID
                + Strings.SQL.IS
                + post.getId()
                + Strings.SQL.SEMICOLON;
        log.debug("method: updatePost post: " + post);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public Post[] getPosts() {
        ResultSet resultSet = selectPostResultSet();
        List<Post> postList = new ArrayList<Post>();
        log.debug("method: getPosts");
        Post post;

        try {
            resultSet.beforeFirst();
            resultSet.next();
            while (!resultSet.isAfterLast()) {
                post = new Post();
                post.setId(resultSet.getInt(DatabaseSchema.POSTS.F_ID));
                post.setName(resultSet.getString(DatabaseSchema.POSTS.F_NAME));
                postList.add(post);
                resultSet.next();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }

        return postList.toArray(new Post[postList.size()]);
    }

    @Override
    public Post getPost(int postId) {
        String query = Strings.SQL.SELECT
                + Strings.SQL.ALL
                + Strings.SQL.FROM
                + DatabaseSchema.TABLE_POSTS
                + Strings.SQL.WHERE
                + DatabaseSchema.POSTS.F_ID
                + Strings.SQL.IS
                + postId;
        log.debug("method: getPost postId: " + postId);
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        Post post = null;
        try {
            resultSet.beforeFirst();
            if(resultSet.next()) {
                post = new Post();
                post.setId(postId);
                post.setName(resultSet.getString(DatabaseSchema.POSTS.F_NAME));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return post;
    }
}
