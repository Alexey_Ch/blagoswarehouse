package com.blagoseng.gui.models;

import com.blagoseng.dao.interfaces.Accessable;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 10.06.13
 * Time: 0:52
 */
public class AccessableTableModel extends AbstractTableModel {
    public AccessableTableModel(List<Accessable> list) {
        this.list = list;
    }

    public AccessableTableModel(List<Accessable> list, String[] columnNames) {
        this.list = list;
        this.columnNames = columnNames;

    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length - 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return list.get(rowIndex).getPropertyByIndex(columnIndex + 1);
    }

    @Override
    public String getColumnName(int index) {
        return columnNames[index];
    }

    public Object getObjectAtRow(int row) {
        return list.get(row);
    }

    public void remove(Accessable o) {
        if(list.remove(o)) fireTableDataChanged();
    }

    private String[] columnNames;
    private List<Accessable> list;
}
