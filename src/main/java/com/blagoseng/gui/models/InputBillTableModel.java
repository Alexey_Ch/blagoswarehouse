package com.blagoseng.gui.models;

import com.blagoseng.dao.model.InputBill;
import com.blagoseng.dao.model.Lists;

import javax.swing.table.AbstractTableModel;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 09.07.13
 * Time: 9:20
 */

public class InputBillTableModel extends AbstractTableModel {
    public InputBillTableModel(InputBill bill) {
        this.bill = bill;
    }

    private InputBill bill;

    @Override
    public int getRowCount() {
        return bill.getListsList().size();
    }

    @Override
    public String getColumnName(int column) {
        String columnName;

        switch (column) {
            case 0: { columnName = "Товар"; break; }
            case 1: { columnName = "Количество"; break; }
            case 2: { columnName = "Ед. изм."; break; }
            default:  columnName = "";
        }
        return columnName;
    }

    @Override
    public int getColumnCount() {
        return 3; // Product  count and units;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: return bill.getListsList().get(rowIndex).getItemId();
            case 2: return bill.getListsList().get(rowIndex).getAmount();
            default: return null;
        }
    }

    public void addLists(Lists order) {
        boolean added = false;
        for(Lists o : bill.getListsList()) {
            if(o.getItemId() == order.getItemId()) {
                o.setAmount(o.getAmount() + order.getAmount());
                added = true;
            }
        }
        if(!added) {
            bill.getListsList().add(order);
        }
        fireTableDataChanged();
    }
}
