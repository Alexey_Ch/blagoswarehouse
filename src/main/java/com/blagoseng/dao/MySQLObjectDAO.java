package com.blagoseng.dao;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.model.Object;
import com.blagoseng.dao.interfaces.ObjectDAO;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:08
 */
public class MySQLObjectDAO implements ObjectDAO {
    private static final Logger LOG = Logger.getLogger(MySQLObjectDAO.class.getName());

    public ResultSet selectObjectResultSet() {
            String query = Strings.SQL.SELECT +
                    Strings.SQL.ALL +
                    Strings.SQL.FROM +
                    DatabaseSchema.TABLE_OBJECTS +
                    Strings.SQL.SEMICOLON;
        Connection conn;
        conn = ((MySQLDAOFactory) DAOFactory.getDAOFactory(DAOFactory.MYSQL)).getConnection();

        ResultSet resultSet = null;
        try {

            Statement statement = conn.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return resultSet;
    }

    @Override
    public boolean createObject(Object object) {
        String query = Strings.SQL.INSERT
                + DatabaseSchema.TABLE_OBJECTS
                + Strings.SQL.START_BRACKET
                + DatabaseSchema.OBJECTS.F_NAME
                + Strings.SQL.END_BRACKET
                + Strings.SQL.VALUES
                + Strings.SQL.BACK_QUOTE
                + object.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.END_BRACKET
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: createObject object: " + object);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean deleteObject(int objectId) {
        String query = Strings.SQL.DELETE
                + DatabaseSchema.TABLE_OBJECTS
                + Strings.SQL.WHERE
                + DatabaseSchema.OBJECTS.F_ID
                + Strings.SQL.IS + objectId
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: deleteObject objectId: " + objectId);
        return DatabaseUtils.executeUpdateQuery(query);
    }


    @Override
    public boolean updateObject(Object object) {
        String query = Strings.SQL.UPDATE
                + DatabaseSchema.TABLE_OBJECTS
                + Strings.SQL.SET
                + DatabaseSchema.OBJECTS.F_NAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + object.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.WHERE
                + DatabaseSchema.OBJECTS.F_ID
                + Strings.SQL.IS
                + object.getId()
                + Strings.SQL.SEMICOLON;
        LOG.debug("method: deleteObject objectId: " + object);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public Object getObject(int objectId) {
        String query = "select * from Objects where object_id =" + objectId + ";";
        Object object = null;
        ResultSet resultSet = DatabaseUtils.executeQuery(query);
        try {
            resultSet.beforeFirst();
            if(resultSet.next()) {
                object = new Object();
                object.setId(objectId);
                object.setName(resultSet.getString(DatabaseSchema.OBJECTS.F_NAME));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }


        return object;
    }

    @Override
    public Object[] getObjects() {
        ResultSet resultSet = selectObjectResultSet();
        List<Object> objectList = new ArrayList<Object>();
        LOG.debug("method: getObjects");
        Object object;

        try {
            resultSet.beforeFirst();
            resultSet.next();
            while (!resultSet.isAfterLast()) {
                object = new Object();
                object.setId(resultSet.getInt(DatabaseSchema.OBJECTS.F_ID));
                object.setName(resultSet.getString(DatabaseSchema.OBJECTS.F_NAME));
                objectList.add(object);
                resultSet.next();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }

        return objectList.toArray(new Object[objectList.size()]);
    }
}
