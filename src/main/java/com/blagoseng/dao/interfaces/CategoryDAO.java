package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.Category;

import java.sql.ResultSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 13:02
 */
public interface CategoryDAO {
    public boolean insertCategory(Category category);
    public boolean insertCategory(String categoryName);
    public List<Category> getAllCategories();
    public Category findCategory(String categoryName);
    public Category findCategory(int categoryId);
    public boolean deleteCategory(String categoryName);
    public boolean deleteCategory(int categoryId);
    public boolean updateCategory(Category category);
    public ResultSet getCategoryResultSet();
    public Category getCategory(int categoryId);
}
