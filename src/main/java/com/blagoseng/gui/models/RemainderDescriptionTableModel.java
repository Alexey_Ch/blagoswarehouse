package com.blagoseng.gui.models;

import com.blagoseng.dao.DAOFactory;
import com.blagoseng.dao.MySQLDAOFactory;
import com.blagoseng.dao.model.RemainderDescription;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 11.07.13
 * Time: 20:56
 */
public class RemainderDescriptionTableModel extends AbstractTableModel {
    public RemainderDescriptionTableModel(int itemId) {
        list = new ArrayList<>();
        factory = DAOFactory.getMySQLDAOFactory();
        loadList(itemId);
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = new Object();
        switch (columnIndex) {
            case 0: { object = factory.getObjectDAO().getObject(list.get(rowIndex).getDestination()).getName(); break;}
            case 1: {object = list.get(rowIndex).getAmount(); break; }
        }
        return object;
    }

    @Override
    public String getColumnName(int column) {
        String colulmName = "";
        switch (column) {
            case 0: {colulmName = "Назначение"; break;}
            case 1: {colulmName = "Количество"; break;}
        }
        return colulmName;
    }

    private void loadList(int itemId) {
        list = factory.getRemainderDescriptionDAO().getRemainderDescriptionList(itemId);
    }

    private List<RemainderDescription> list;
    private MySQLDAOFactory factory;
}
