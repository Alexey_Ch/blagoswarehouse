package com.blagoseng.dao.interfaces;

import com.blagoseng.dao.model.InputBill;
import com.blagoseng.dao.model.Lists;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 10.07.13
 * Time: 2:22
 */
public interface ListDAO {
    public int createList(Lists lists);
    public boolean updateList(Lists lists);
    public boolean deleteList(int listId);
    public InputBill getLists(int listId);
    public List<Lists> getLists();
}
