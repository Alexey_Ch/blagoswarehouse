package com.blagoseng.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: enjoy
 * Date: 03.07.13
 * Time: 12:48
 */
public interface HasName {
    public String getName();
}
