package com.blagoseng.dao;

import com.blagoseng.constants.strings.Strings;
import com.blagoseng.dao.interfaces.EmployerDAO;
import com.blagoseng.dao.model.Employer;
import com.blagoseng.utils.DatabaseSchema;
import com.blagoseng.utils.DatabaseUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 08.06.13
 * Time: 14:06
 */
public class MySQLEmployerDAO implements EmployerDAO {
    private static Logger log = Logger.getLogger(MySQLUnitDAO.class.getName());

    @Override
    public ResultSet selectEmployerResultSet() {
        String query = Strings.SQL.SELECT +
                Strings.SQL.ALL +
                Strings.SQL.FROM +
                DatabaseSchema.TABLE_EMPLOYERS +
                Strings.SQL.SEMICOLON;

        Connection conn;
        conn = ((MySQLDAOFactory) DAOFactory.getDAOFactory(DAOFactory.MYSQL)).getConnection();

        ResultSet resultSet = null;
        try {
            Statement statement = conn.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    @Override
    public void insertEmployer(Employer employer) {
        String query = Strings.SQL.INSERT
                + DatabaseSchema.TABLE_EMPLOYERS
                + Strings.SQL.START_BRACKET
                + DatabaseSchema.EMPLOYERS.F_NAME
                + Strings.SQL.COMMA
                + DatabaseSchema.EMPLOYERS.F_SURNAME
                + Strings.SQL.COMMA
                + DatabaseSchema.EMPLOYERS.F_PATRONYMIC
                + Strings.SQL.COMMA
                + DatabaseSchema.EMPLOYERS.F_ACCESS_LEVEL
                + Strings.SQL.COMMA
                + DatabaseSchema.EMPLOYERS.F_POST_ID

                + Strings.SQL.END_BRACKET
                + Strings.SQL.VALUES
                + Strings.SQL.BACK_QUOTE
                + employer.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA

                + Strings.SQL.BACK_QUOTE
                + employer.getSurname()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA

                + Strings.SQL.BACK_QUOTE
                + employer.getPatronymic()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA

                + employer.getAccessLevel()
                + Strings.SQL.COMMA
                + employer.getPostId()

                + Strings.SQL.END_BRACKET
                + Strings.SQL.SEMICOLON;
        log.debug("method: insertEmployer Employer: " + employer);
        DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean deleteEmployer(int employerId) {
        String query = Strings.SQL.DELETE
                + DatabaseSchema.TABLE_EMPLOYERS
                + Strings.SQL.WHERE
                + DatabaseSchema.EMPLOYERS.F_ID
                + Strings.SQL.IS
                + employerId;
        log.debug("method: deleteEmployer - employerId = " + employerId);
        return DatabaseUtils.executeUpdateQuery(query);
    }

    @Override
    public boolean updateEmployer(Employer employer) {
        String query = Strings.SQL.UPDATE
                + DatabaseSchema.TABLE_EMPLOYERS
                + Strings.SQL.SET

                + DatabaseSchema.EMPLOYERS.F_NAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + employer.getName()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA

                + DatabaseSchema.EMPLOYERS.F_SURNAME
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + employer.getSurname()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA

                + DatabaseSchema.EMPLOYERS.F_PATRONYMIC
                + Strings.SQL.IS
                + Strings.SQL.BACK_QUOTE
                + employer.getPatronymic()
                + Strings.SQL.BACK_QUOTE
                + Strings.SQL.COMMA

                + DatabaseSchema.EMPLOYERS.F_ACCESS_LEVEL
                + Strings.SQL.IS
                + employer.getAccessLevel()
                + Strings.SQL.COMMA

                + DatabaseSchema.EMPLOYERS.F_POST_ID
                + Strings.SQL.IS
                + employer.getPostId()

                + Strings.SQL.WHERE
                + DatabaseSchema.EMPLOYERS.F_ID
                + Strings.SQL.IS
                + employer.getId();
        log.debug("method: updateEmployer - employerId = " + employer.getId());
        return DatabaseUtils.executeUpdateQuery(query);
    }
}
